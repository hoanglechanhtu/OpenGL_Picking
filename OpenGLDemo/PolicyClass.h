#pragma once

template <class T>
struct OpNewCreator {
	static T* Create() {
		return new T;
	}

};

template <class T> 
struct MallocCreator{
	static T* Create() {
		void* buf = std::malloc(sizeof(T));
		if (!buf) return 0;
		return new(buf) T;
	}
};

template  <class  T>
struct PrototypeCreator {
	PrototypeCreator(T* pObj = 0):pPrototype_(pObj) {
			
	}

	T* Create() {
		return pPrototype_ ? pPrototype_->Clone() : 0;
	}

	T* GetPrototype() { return pPrototype_; }

	void SetPrototype(T* obj) { pPrototype_ = obj; }
private:
	T* pPrototype_;

};

//Library code
//host class

template <template<class Created> class CreationPolicy>
class MeshManager : public CreationPolicy<Mesh> {
public:

	void DoSomething() {
		Gadget* pW = CreationPolicy<Gadget>().Create();
	}

};


class PolicyClass
{
public:
	PolicyClass();
	~PolicyClass();
};

