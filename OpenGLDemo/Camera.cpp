#include "Camera.h"



Camera::Camera()
{
	cameraPosition = glm::vec3(20.0f, 10.0f, 5.0f);
	cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
	cameraUp = glm::vec3(0.0f, 1.0f, 0.0f);
	cameraSpeed = 0.5f;
}


Camera::~Camera()
{
}

void Camera::setCameraSpeed(float value)
{
	cameraSpeed = value;
}

float Camera::getCameraSpeed()
{
	return cameraSpeed;
}

void Camera::adjustCameraPosition(glm::vec3 &value)
{
	cameraPosition += value;
}

void Camera::adjustCameraFront(glm::vec3 & value)
{
	cameraFront += value;
}

void Camera::adjustCameraUp(glm::vec3 & value)
{
	 cameraUp += value;
}

glm::vec3 Camera::getCameraPosition()
{
	return cameraPosition;
}

glm::vec3 Camera::getCameraUp()
{
	return glm::normalize(cameraUp)   ;
}

glm::vec3 Camera::getCameraFront()
{
	return  glm::normalize(cameraFront);
}

glm::vec3 Camera::getCameraRight()
{
	glm::vec3 cameraRight = glm::cross(cameraUp, cameraFront);

	return glm::normalize(cameraRight);
}

void Camera::setCameraFront(glm::vec3 value)
{
	cameraFront = value;
}

void Camera::setCameraPosition(glm::vec3 value)
{
	cameraPosition = value;
}

void Camera::setCameraUp(glm::vec3 value)
{
	cameraUp = value;
}

void Camera::returnToDefaultSetting()
{
	cameraPosition = glm::vec3(10.0f, 10.0f, 10.0f);
	cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
	cameraUp = glm::vec3(0.0f, 1.0f, 0.0f);
	cameraSpeed = 0.5f;
}

void Camera::setFrontView()
{
	cameraPosition = glm::vec3(0.0, 0.0f, 10.0f);
	cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
	cameraUp = glm::vec3(0.0f, 1.0f, 0.0f);
	cameraSpeed = 0.5f;
}

void Camera::setBackView()
{
	cameraPosition = glm::vec3(0.0f, 10.0f, -10.0f);
	cameraFront = glm::vec3(0.0f, 0.0f, 1.0f);
	cameraUp = glm::vec3(0.0f, 1.0f, 0.0f);
	cameraSpeed = 0.5f;
}

void Camera::setTopView()
{
	cameraPosition = glm::vec3(0.0f, 10.0f, 0.0f);
	cameraFront = glm::vec3(0.0f, -1.0f, 0.0f);
	cameraUp = glm::vec3(1.0f, 0.0f, 0.0f);
	cameraSpeed = 0.5f;
}

void Camera::setDownView()
{
	cameraPosition = glm::vec3(0.0f, -10.0f, 0.0f);
	cameraFront = glm::vec3(0.0f, 1.0f, 0.0f);
	cameraUp = glm::vec3(1.0f, 0.0f, 0.0f);
	cameraSpeed = 0.5f;

}

void Camera::setRightView()
{
	cameraPosition = glm::vec3(10.0f, 0.0f, 0.0f);
	cameraFront = glm::vec3(-1.0f, 0.0f, 0.0f);
	cameraUp = glm::vec3(0.0f, 1.0f, 0.0f);
	cameraSpeed = 0.5f;

}

void Camera::setLeftView()
{
	cameraPosition = glm::vec3(-10.0f, 0.0f, 0.0f);
	cameraFront = glm::vec3(1.0f, 0.0f, 0.0f);
	cameraUp = glm::vec3(0.0f, 1.0f, 0.0f);
	cameraSpeed = 0.5f;

}
