#pragma once

 

#include <math.h>
#include "glm/glm.hpp"
#include<glut.h>
#define MAX 100000
#define MIN -100000

 
class AABB3 {
public:
	
	glm::vec3 min;
	glm::vec3 max;
	glm::vec3 tempMin;
	glm::vec3 tempMax;
	float hx, hy, hz;

	glm::vec3 center;
	
	void empty() {
		min.x = min.y = min.z = MAX;
		max.x = max.y = max.z = MIN;


	}
	//add single point to AABB box
	void add(const glm::vec3 &p) {
		if (p.x < min.x) min.x = p.x;
		if (p.x > max.x) max.x = p.x;
		if (p.y < min.y) min.y = p.y;
		if (p.y > max.y) max.y = p.y;
		if (p.z < min.z) min.z = p.z;
		if (p.z > max.z) max.z = p.z;
		tempMax = max;
		tempMin = min;
	}
 //TODO remove race condition
	void calcuateDistance() {
		hx = (tempMax.x - center.x);
		hx = (hx < 0) ? (-hx) : hx;
		hy = tempMax.y - center.y;
		hy = (hy < 0) ? (-hy) : hy;
		hz = tempMax.z - center.z;
		hz = (hz < 0) ? (-hz) : hz;
	}

	void calculateCenter() {
		glm::vec3 temp = glm::vec3(1);
		temp.x = (tempMin.x + tempMax.x) / 2;
		temp.y = (tempMin.y + tempMax.y) / 2;
		temp.z = (tempMin.z + tempMax.z) / 2;
		center = temp;
	}
	bool RayABBBIntersect(glm::vec3& o, glm::vec3& d,float& value) {
		float tmin = MIN;
		float tmax = MAX;
		 
		glm::vec3 p = center - o;//   calculate p = Ac-o
		// hash code
		//i = x
		{
			float e = p.x;
			float f = d.x;

			if (f != 0) {
				float t1 = (e + hx) / f;
				float t2 = (e - hx) / f;
				if (t1 > t2) {
					float temp = t2;
					t2 = t1;
					t1 = temp;
				}

				if (t1 > tmin) tmin = t1; // max(min)
				if (t2 < tmax) tmax = t2; // min(max)
				if (tmin > tmax) return false; // missed
				if (tmax < 0) return false; // the box is behind origin of ray

			}
			else if ((-e - hx > 0) || (-e + hx < 0)) return false;

		}
		
		//TODO i=y
		{	
			float e = p.y;
			float f = d.y;

		if (f != 0) {
			float t1 = (e + hy) / f;
			float t2 = (e - hy) / f;
			if (t1 > t2) {
				float temp = t2;
				t2 = t1;
				t1 = temp;
			}

			if (t1 > tmin) tmin = t1; // max(min)
			if (t2 < tmax) tmax = t2; // min(max)
			if (tmin > tmax) return false; // missed
			if (tmax < 0) return false; // the box is behind origin of ray

		}
		else if ((-e - hy > 0) || (-e + hy < 0)) return false; }
		//TODO i=z
		{
			float e = p.z;
			float f = d.z;

			if (f != 0) {
				float t1 = (e + hz) / f;
				float t2 = (e - hz) / f;
				if (t1 > t2) {
					float temp = t2;
					t2 = t1;
					t1 = temp;
				}

				if (t1 > tmin) tmin = t1; // max(min)
				if (t2 < tmax) tmax = t2; // min(max)
				if (tmin > tmax) return false; // missed
				if (tmax < 0) return false; // the box is behind origin of ray

			}
			else if ((-e - hz > 0) || (-e + hz < 0)) return false;
		}

		if (tmin > 0) { 
			value = tmin;
			return true; 
		
		}
		else {
			value = tmax;
			return true;
		}
		
	}
	
	void setToTransformedBox(const glm::mat4x3 &m) {
		 
		tempMin = tempMax = glm::vec3(m[3][0], m[3][1], m[3][2]);


		if (m[0][0] > 0.0f) {
			tempMin.x += m[0][0] * min.x;
			tempMax.x += m[0][0] * max.x;
		}
		else {
			tempMin.x += m[0][0] * max.x;
			tempMax.x += m[0][0] * min.x;
		}
			
		if (m[0][1] > 0.0f) {
			tempMin.y += m[0][1] * min.x;
			tempMax.y += m[0][1] * max.x;
		}
		else {
			tempMin.y += m[0][1] * max.x;
			tempMax.y += m[0][1] * min.x;
		}
	

		if (m[0][2] > 0.0f) {
			tempMin.z += m[0][2] * min.x;
			tempMax.z += m[0][2] * max.x;
		}
		else {
			tempMin.z += m[0][2] * max.x;
			tempMax.z += m[0][2] * min.x;
		}

		// seccond row


		if (m[1][0] > 0.0f) {
			tempMin.x += m[1][0] * min.y;
			tempMax.x += m[1][0] * max.y;
		}
		else {
			tempMin.x += m[1][0] * max.y;
			tempMax.x += m[1][0] * min.y;
		}

		if (m[1][1] > 0.0f) {
			tempMin.y += m[1][1] * min.y;
			tempMax.y += m[1][1] * max.y;
		}
		else {
			tempMin.y += m[1][1] * max.y;
			tempMax.y += m[1][1] * min.y;
		}


		if (m[1][2] > 0.0f) {
			tempMin.z += m[1][2] * min.y;
			tempMax.z += m[1][2] * max.y;
		}
		else {
			tempMin.z += m[1][2] * max.y;
			tempMax.z += m[1][2] * min.y;
		}


		//third row


		if (m[2][0] > 0.0f) {
			tempMin.x += m[2][0] * min.z;
			tempMax.x += m[2][0] * max.z;
		}
		else {
			tempMin.x += m[2][0] * max.z;
			tempMax.x += m[2][0] * min.z;
		}

		if (m[2][1] > 0.0f) {
			tempMin.y += m[2][1] * min.z;
			tempMax.y += m[2][1] * max.z;
		}
		else {
			tempMin.y += m[2][1] * max.z;
			tempMax.y += m[2][1] * min.z;
		}


		if (m[2][2] > 0.0f) {
			tempMin.z += m[2][2] * min.z;
			tempMax.z += m[2][2] * max.z;
		}
		else {
			tempMin.z += m[2][2] * max.z;
			tempMax.z += m[2][2] * min.z;
		}

	
	}
};