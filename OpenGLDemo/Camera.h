#pragma once
#include "glm/glm.hpp"
class Camera
{
public:
	Camera();
	~Camera();
	void setCameraSpeed(float value);
	float getCameraSpeed();
	//+=
	void adjustCameraPosition(glm::vec3 &value);
	void adjustCameraFront(glm::vec3 &value);
	void adjustCameraUp(glm::vec3 &value);
	glm::vec3 getCameraPosition();
	glm::vec3 getCameraUp();
	glm::vec3 getCameraFront();
	glm::vec3 getCameraRight();
	void setCameraFront(glm::vec3  value);
	void setCameraPosition(glm::vec3 value);
	void setCameraUp(glm::vec3 value);
	void returnToDefaultSetting();
	void setFrontView();
	void setBackView();
	void setTopView();
	void setDownView();
	void setRightView();
	void setLeftView();
private:

	glm::vec3 cameraPosition;
	glm::vec3 cameraFront;
	glm::vec3 cameraUp;
	float cameraSpeed;
};

