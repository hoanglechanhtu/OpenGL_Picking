#include "TriangleMesh.h"



TriangleMesh::TriangleMesh()
{
}


TriangleMesh::~TriangleMesh()
{
}


void TriangleMesh::createSt(float fSize) {
	int i;

	vertexCount = 8;
	vertexList = new Vertex[vertexCount];

	vertexList[0].pos = glm::vec3(-fSize, fSize, fSize);
	vertexList[1].pos = glm::vec3(fSize, fSize, fSize);
	vertexList[2].pos = glm::vec3(fSize, fSize, -fSize);
	vertexList[3].pos = glm::vec3(-fSize, fSize, -fSize);
	vertexList[4].pos = glm::vec3(-fSize, -fSize, fSize);
	vertexList[5].pos = glm::vec3(fSize, -fSize, fSize);
	vertexList[6].pos = glm::vec3(fSize, -fSize, -fSize);
	vertexList[7].pos = glm::vec3(-fSize, -fSize, -fSize);


	triangleCount = 12;
	triangleList = new Triangle[triangleCount];

	//Left face
	triangleList[0].vertexIndex[0] = 0;
	triangleList[0].vertexIndex[1] = 7;
	triangleList[0].vertexIndex[2] = 3;

	triangleList[1].vertexIndex[0] = 0;
	triangleList[1].vertexIndex[1] = 4;
	triangleList[1].vertexIndex[2] = 7;

	//right face
	triangleList[2].vertexIndex[0] = 1;
	triangleList[2].vertexIndex[1] = 2;
	triangleList[2].vertexIndex[2] = 6;

	triangleList[3].vertexIndex[0] = 0;
	triangleList[3].vertexIndex[1] = 6;
	triangleList[3].vertexIndex[2] = 5;

	//up face

	triangleList[4].vertexIndex[0] = 3;
	triangleList[4].vertexIndex[1] = 2;
	triangleList[4].vertexIndex[2] = 0;

	triangleList[5].vertexIndex[0] = 0;
	triangleList[5].vertexIndex[1] = 2;
	triangleList[5].vertexIndex[2] = 1;


	// down face

	triangleList[6].vertexIndex[0] = 4;
	triangleList[6].vertexIndex[1] = 5;
	triangleList[6].vertexIndex[2] = 6;

	triangleList[7].vertexIndex[0] = 4;
	triangleList[7].vertexIndex[1] = 6;
	triangleList[7].vertexIndex[2] = 7;

	//front face

	triangleList[8].vertexIndex[0] = 0;
	triangleList[8].vertexIndex[1] = 1;
	triangleList[8].vertexIndex[2] = 5;

	triangleList[9].vertexIndex[0] = 0;
	triangleList[9].vertexIndex[1] = 5;
	triangleList[9].vertexIndex[2] = 4;

	//behind face
	triangleList[10].vertexIndex[0] = 2;
	triangleList[10].vertexIndex[1] = 6;
	triangleList[10].vertexIndex[2] = 3;

	triangleList[11].vertexIndex[0] = 3;
	triangleList[11].vertexIndex[1] = 6;
	triangleList[11].vertexIndex[2] = 7;


}

void TriangleMesh::draw() {
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	for (int f = 0; f < triangleCount; f++)
	{
		glBegin(GL_TRIANGLES);
		for (int v = 0; v < 3; v++)
		{
			int		iv = triangleList[f].vertexIndex[v];

			glVertex3f(vertexList[iv].pos.x, vertexList[iv].pos.y, vertexList[iv].pos.z);
		}
		glEnd();
	}
}