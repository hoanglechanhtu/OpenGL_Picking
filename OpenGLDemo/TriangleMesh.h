#pragma once
#include"glm/glm.hpp"
#include "glut.h"
struct Vertex {
	//3d position of the vertex
	glm::vec3 pos;
	//orther information could include
	//texture mapping coordinate
	//surface normal,lighting value

};

struct Triangle {
	int vertexIndex[3];
	//Orther information could include
	//a normal, matierial information

};
class TriangleMesh
{
public:
	TriangleMesh();
	~TriangleMesh();
	int vertexCount;
	Vertex *vertexList;
	int triangleCount;
	Triangle *triangleList;
	void draw();
	void createSt(float fSize);

};
