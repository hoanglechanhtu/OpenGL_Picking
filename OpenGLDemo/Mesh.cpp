
#include "Mesh.h"

#include <math.h>
#include "AABB.h"
#define Deg2Rad (3.1415926/180)
#define PI			3.1415926
#define	COLORNUM		14
#define OXY 0
#define OYZ 1
#define OXZ 2




//TODO create policy class to create Mesh
//TODO change to effective way to store mesh

float	ColorArr[COLORNUM][3] = {{1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, { 0.0,  0.0, 1.0}, 
								{1.0, 1.0,  0.0}, { 1.0, 0.0, 1.0},{ 0.0, 1.0, 1.0}, 
								 {0.3, 0.3, 0.3}, {0.5, 0.5, 0.5}, { 0.9,  0.9, 0.9},
								{1.0, 0.5,  0.5}, { 0.5, 1.0, 0.5},{ 0.5, 0.5, 1.0},
									{0.0, 0.0, 0.0}, {1.0, 1.0, 1.0}};



void Mesh::SetColor(int colorIdx) {
	for (int f = 0; f < numFaces; f++) {
		for (int v = 0; v < face[f].nVerts; v++) {
			face[f].vert[v].colorIndex = colorIdx;
		}
	}
	colorIndex = colorIdx;
}

// new draw with lighting

void Mesh::CreateUVSphere(float radius) {
	float h[19], r[19];
	for (int i = 1; i < 20; i++)
	{
		h[i - 1] = radius * sin(Deg2Rad * ((i * 9 + 270) % 360));
		r[i - 1] = abs(radius *cos(Deg2Rad * ((i * 9 + 270) % 360)));
		//cout << h[i - 1] << " " << r[i - 1] << endl;
		//system("pause");
	}
	//system("pause");
	numVerts = 382;
	pt = new glm::vec3[numVerts];
	pt[380] = glm::vec3(0, -radius, 0);
	pt[381] = glm::vec3(0, radius, 0);
	for (int i = 0; i < 19; i++)
	{
		for (int j = 0; j < 20; j++)
		{
			pt[j + i * 20] = glm::vec3(r[i] * sin(j * (360 / 20) * Deg2Rad), h[i], r[i] * cos(j * (360 / 20) * Deg2Rad));

		}
	}


	numFaces = 400;
	face = new Face[numFaces];



	for (int i = 0; i < 18; i++)
	{
		for (int j = 0; j < 20; j++)
		{
			face[j + i * 20].nVerts = 4;
			face[j + i * 20].vert = new VertexID[face[j + i * 20].nVerts];
			face[j + i * 20].vert[0].vertIndex = j + 20 * i;
			face[j + i * 20].vert[1].vertIndex = (j + 1) % 20 + 20 * i;
			face[j + i * 20].vert[2].vertIndex = (j + 1) % 20 + 20 * ((i + 1));
			face[j + i * 20].vert[3].vertIndex = j + 20 * ((i + 1));

		}
	}

	// ve 2 day
	for (int i = 0; i < 20; i++)
	{
		// day tren
		face[380 + i].nVerts = 3;
		face[380 + i].vert = new VertexID[face[380 + i].nVerts];
		face[380 + i].vert[0].vertIndex = 381;
		face[380 + i].vert[1].vertIndex = i + 360;
		face[380 + i].vert[2].vertIndex = (i + 1) % 20 + 360;



		// day duoi
		face[360 + i].nVerts = 3;
		face[360 + i].vert = new VertexID[face[360 + i].nVerts];
		face[360 + i].vert[0].vertIndex = 380;
		face[360 + i].vert[1].vertIndex = (i + 1) % 20;
		face[360 + i].vert[2].vertIndex = i;


	}
	CalculateFacesNorm();
	addAllPoint();
	box->calculateCenter();
	box->calcuateDistance();

}
void Mesh::CreateTorus(float radius, float thickness) {
	float r[20], h[20];

	for (int i = 0; i < 20; i++)
	{
		h[i] = (thickness / 2.0) * sin((360 / 20) * i * Deg2Rad);
		r[i] = radius + (thickness / 2.0) * cos((360 / 20) * i * Deg2Rad);

	}
	numVerts = 400;
	pt = new glm::vec3[numVerts];
	//pt[400].set(0, 0, 0);
	for (int i = 0; i < 20; i++)
	{
		for (int j = 0; j < 20; j++)
		{
			pt[j + i * 20] = glm::vec3(r[i] * sin(j * (360 / 20) * Deg2Rad), h[i], r[i] * cos(j * (360 / 20) * Deg2Rad));

		}

	}

	numFaces = 400;
	face = new Face[numFaces];
	int i = 0;
	int j = 0;



	for (int i = 0; i < 20; i++)
	{
		for (int j = 0; j < 20; j++)
		{
			face[j + i * 20].nVerts = 4;
			face[j + i * 20].vert = new VertexID[face[j + i * 20].nVerts];
			face[j + i * 20].vert[0].vertIndex = j + 20 * i;
			face[j + i * 20].vert[1].vertIndex = (j + 1) % 20 + 20 * i;
			face[j + i * 20].vert[2].vertIndex = (j + 1) % 20 + 20 * ((i + 1) % 20);
			face[j + i * 20].vert[3].vertIndex = j + 20 * ((i + 1) % 20);
		}
	}

	CalculateFacesNorm();
	addAllPoint();
	box->calculateCenter();
	box->calcuateDistance();
}
void Mesh::CreateCube(float	fSizeX, float fSizeY, float	fSizeZ) {
	int i;

	numVerts = 8;
	pt = new glm::vec3[numVerts];
	pt[0] = glm::vec3(-fSizeX, fSizeY, fSizeZ);
	pt[1] = glm::vec3(fSizeX, fSizeY, fSizeZ);
	pt[2] = glm::vec3(fSizeX, fSizeY, -fSizeZ);
	pt[3] = glm::vec3(-fSizeX, fSizeY, -fSizeZ);
	pt[4] = glm::vec3(-fSizeX, -fSizeY, fSizeZ);
	pt[5] = glm::vec3(fSizeX, -fSizeY, fSizeZ);
	pt[6] = glm::vec3(fSizeX, -fSizeY, -fSizeZ);
	pt[7] = glm::vec3(-fSizeX, -fSizeY, -fSizeZ);


	numFaces = 6;
	face = new Face[numFaces];

	//Left face
	face[0].nVerts = 4;
	face[0].vert = new VertexID[face[0].nVerts];
	face[0].vert[0].vertIndex = 1;
	face[0].vert[1].vertIndex = 5;
	face[0].vert[2].vertIndex = 6;
	face[0].vert[3].vertIndex = 2;


	//Right face
	face[1].nVerts = 4;
	face[1].vert = new VertexID[face[1].nVerts];
	face[1].vert[0].vertIndex = 0;
	face[1].vert[1].vertIndex = 3;
	face[1].vert[2].vertIndex = 7;
	face[1].vert[3].vertIndex = 4;


	//top face
	face[2].nVerts = 4;
	face[2].vert = new VertexID[face[2].nVerts];
	face[2].vert[0].vertIndex = 0;
	face[2].vert[1].vertIndex = 1;
	face[2].vert[2].vertIndex = 2;
	face[2].vert[3].vertIndex = 3;


	//bottom face
	face[3].nVerts = 4;
	face[3].vert = new VertexID[face[3].nVerts];
	face[3].vert[0].vertIndex = 7;
	face[3].vert[1].vertIndex = 6;
	face[3].vert[2].vertIndex = 5;
	face[3].vert[3].vertIndex = 4;


	//near face
	face[4].nVerts = 4;
	face[4].vert = new VertexID[face[4].nVerts];
	face[4].vert[0].vertIndex = 4;
	face[4].vert[1].vertIndex = 5;
	face[4].vert[2].vertIndex = 1;
	face[4].vert[3].vertIndex = 0;

	//Far face
	face[5].nVerts = 4;
	face[5].vert = new VertexID[face[5].nVerts];
	face[5].vert[0].vertIndex = 3;
	face[5].vert[1].vertIndex = 2;
	face[5].vert[2].vertIndex = 6;
	face[5].vert[3].vertIndex = 7;

	CalculateFacesNorm();
	addAllPoint();
	box->calculateCenter();
	box->calcuateDistance();
}
void Mesh::CreateCylinder(float radius, float height) {
	int k;
	numVerts = 42;

	pt = new glm::vec3[numVerts];
	pt[40] = glm::vec3(0.0, height, 0.0);
	pt[41] = glm::vec3(0.0, -height, 0.0);

	float w = 360.0 / 20;

	for (int i = 0; i < 20; i++)
	{
		pt[i] = glm::vec3(radius*cos(i * w * 2 * PI / 360), height, radius*sin(i * w * 2 * PI / 360));
		pt[20 + i] = glm::vec3(radius*cos(i * w * 2 * PI / 360), -height, radius*sin(i * w * 2 * PI / 360));
	}

	// ve mat day
	numFaces = 60;
	face = new Face[numFaces];

	// 2 day
	for (int i = 0; i < 19; i++)
	{
		face[i].nVerts = 3;
		face[i].vert = new VertexID[face[i].nVerts];
		face[i].vert[0].vertIndex = 40;
		face[i].vert[1].vertIndex = i + 1;
		face[i].vert[2].vertIndex = i;


		face[19 + i].nVerts = 3;
		face[19 + i].vert = new VertexID[face[i].nVerts];
		face[19 + i].vert[0].vertIndex = 41;
		face[19 + i].vert[1].vertIndex = 20 + i;
		face[19 + i].vert[2].vertIndex = 20 + i + 1;


	}
	{
		face[38].nVerts = 3;
		face[38].vert = new VertexID[face[38].nVerts];
		face[38].vert[0].vertIndex = 40;
		face[38].vert[1].vertIndex = 1;
		face[38].vert[2].vertIndex = 19;



		face[39].nVerts = 3;
		face[39].vert = new VertexID[face[39].nVerts];
		face[39].vert[0].vertIndex = 41;
		face[39].vert[1].vertIndex = 39;
		face[39].vert[2].vertIndex = 20;


	}

	for (int i = 0; i < 19; i++)
	{
		face[40 + i].nVerts = 4;
		face[40 + i].vert = new VertexID[face[40 + i].nVerts];
		face[40 + i].vert[0].vertIndex = i;
		face[40 + i].vert[1].vertIndex = i + 1;
		face[40 + i].vert[2].vertIndex = 20 + i + 1;
		face[40 + i].vert[3].vertIndex = 20 + i;

	}
	face[59].nVerts = 4;
	face[59].vert = new VertexID[face[38].nVerts];
	face[59].vert[0].vertIndex = 19;
	face[59].vert[1].vertIndex = 1;
	face[59].vert[2].vertIndex = 20;
	face[59].vert[3].vertIndex = 39;

	CalculateFacesNorm();
	addAllPoint();
	box->calculateCenter();
	box->calcuateDistance();
}
void Mesh::CreateCone(float radius, float height) {
	numVerts = 42;
	pt = new glm::vec3[numVerts];
	pt[40] = glm::vec3(0.0, 0.0, 0.0);
	pt[41] = glm::vec3(0, height, 0);
	for (int i = 0; i < 40; i++)
	{
		pt[i] = glm::vec3(radius*cos(9 * i * Deg2Rad), 0.0, radius*sin(9 * i*Deg2Rad));
	}

	numFaces = 80;
	face = new Face[numFaces];
	// 
	for (int i = 0; i < 39; i++)
	{
		// day
		face[i].nVerts = 3;
		face[i].vert = new VertexID[face[i].nVerts];
		face[i].vert[0].vertIndex = 40;
		face[i].vert[1].vertIndex = i;
		face[i].vert[2].vertIndex = i + 1;


		// ben
		face[i + 40].nVerts = 3;
		face[i + 40].vert = new VertexID[face[i + 40].nVerts];
		face[i + 40].vert[0].vertIndex = 41;
		face[i + 40].vert[1].vertIndex = i + 1;
		face[i + 40].vert[2].vertIndex = i;


	}

	face[39].nVerts = 3;
	face[39].vert = new VertexID[face[39].nVerts];
	face[39].vert[0].vertIndex = 40;
	face[39].vert[1].vertIndex = 39;
	face[39].vert[2].vertIndex = 0;


	face[79].nVerts = 3;
	face[79].vert = new VertexID[face[39].nVerts];
	face[79].vert[0].vertIndex = 41;
	face[79].vert[1].vertIndex = 0;
	face[79].vert[2].vertIndex = 39;

	CalculateFacesNorm();
	addAllPoint();
	box->calculateCenter();
	box->calcuateDistance();
}

///////////////////////////////////////////////////////////////////////




void Mesh::CreateCube(float	fSize)
{
	int i;

	numVerts=8;
	pt = new glm::vec3[numVerts];
	 
	pt[0] = glm::vec3(-fSize, fSize, fSize);
	pt[1] = glm::vec3( fSize, fSize, fSize);
	pt[2] = glm::vec3( fSize, fSize, -fSize);
	pt[3] = glm::vec3(-fSize, fSize, -fSize);
	pt[4] = glm::vec3(-fSize, -fSize, fSize);
	pt[5] = glm::vec3( fSize, -fSize, fSize);
	pt[6] = glm::vec3( fSize, -fSize, -fSize);
	pt[7] = glm::vec3(-fSize, -fSize, -fSize);


	numFaces= 6;
	face = new Face[numFaces];

	//Left face
	face[0].nVerts = 4;
	face[0].vert = new VertexID[face[0].nVerts];
	face[0].vert[0].vertIndex = 1;
	face[0].vert[1].vertIndex = 5;
	face[0].vert[2].vertIndex = 6;
	face[0].vert[3].vertIndex = 2;
	for(i = 0; i<face[0].nVerts ; i++)
		face[0].vert[i].colorIndex = 0;
	
	//Right face
	face[1].nVerts = 4;
	face[1].vert = new VertexID[face[1].nVerts];
	face[1].vert[0].vertIndex = 0;
	face[1].vert[1].vertIndex = 3;
	face[1].vert[2].vertIndex = 7;
	face[1].vert[3].vertIndex = 4;
	for(i = 0; i<face[1].nVerts ; i++)
		face[1].vert[i].colorIndex = 1;

	//top face
	face[2].nVerts = 4;
	face[2].vert = new VertexID[face[2].nVerts];
	face[2].vert[0].vertIndex = 0;
	face[2].vert[1].vertIndex = 1;
	face[2].vert[2].vertIndex = 2;
	face[2].vert[3].vertIndex = 3;
	for(i = 0; i<face[2].nVerts ; i++)
		face[2].vert[i].colorIndex = 2;

	//bottom face
	face[3].nVerts = 4;
	face[3].vert = new VertexID[face[3].nVerts];
	face[3].vert[0].vertIndex = 7;
	face[3].vert[1].vertIndex = 6;
	face[3].vert[2].vertIndex = 5;
	face[3].vert[3].vertIndex = 4;
	for(i = 0; i<face[3].nVerts ; i++)
		face[3].vert[i].colorIndex = 3;

	//near face
	face[4].nVerts = 4;
	face[4].vert = new VertexID[face[4].nVerts];
	face[4].vert[0].vertIndex = 4;
	face[4].vert[1].vertIndex = 5;
	face[4].vert[2].vertIndex = 1;
	face[4].vert[3].vertIndex = 0;
	for(i = 0; i<face[4].nVerts ; i++)
		face[4].vert[i].colorIndex = 4;

	//Far face
	face[5].nVerts = 4;
	face[5].vert = new VertexID[face[5].nVerts];
	face[5].vert[0].vertIndex = 3;
	face[5].vert[1].vertIndex = 2;
	face[5].vert[2].vertIndex = 6;
	face[5].vert[3].vertIndex = 7;
	for(i = 0; i<face[5].nVerts ; i++)
		face[5].vert[i].colorIndex = 5;
	CalculateFacesNorm();
	addAllPoint();
	box->calculateCenter();
	box->calcuateDistance();
}





void Mesh::tick()
{
	 
	//DEBUG 
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			std::cout << transMatrix[i][j] << " ";
		}
		std::cout << std::endl;
	}
	
	for (int i = 0; i < numVerts; i++) {
		std::cout << "vertex index: " << i << std::endl;
		std::cout << pt[i].x << " " << pt[i].y << " " << pt[i].z <<std::endl;
	}
	std::cout << "Vertex position in world space " << std::endl;
	for (int i = 0; i < numVerts; i++) {
		std::cout << "vertex index: " << i << std::endl;
		glm::vec4 temp = transMatrix*glm::vec4(pt[i],1);
		std::cout << temp.x << " " << temp.y << " " << temp.z << std::endl;
	}
	/* 
	std::cout << "Normal vector" << std::endl;
	for (int i = 0; i < numFaces; i++) {
		std::cout << "Face index " << i<<std::endl;
		for (int j = 0; j < face[i].nVerts; j++) {
			std::cout << "vertex index: " << face[i].vert[j].vertIndex << std::endl;
			 
			std::cout << pt[face[i].vert[j].vertIndex].x << " " << pt[face[i].vert[j].vertIndex].y << " " << pt[face[i].vert[j].vertIndex].z << std::endl;
		}
		glm::vec4 localSpace0 = glm::vec4(pt[face[i].vert[0].vertIndex], 1);

		 
		glm::vec4 vertex0InWorldSpace = transMatrix*localSpace0;
		glm::vec3 _n = glm::vec3(0, 0, 0);

		for (int k = 0; k <face[i].nVerts; k++) {
			glm::vec4 first = transMatrix*glm::vec4(pt[face[i].vert[k].vertIndex], 1);
			glm::vec4 sec = transMatrix*glm::vec4(pt[face[i].vert[(k+1) % face[i].nVerts].vertIndex], 1);

			_n.x += (first.y - sec.y)*(first.z + sec.z);
			_n.y += (first.z - sec.z)*(first.x + sec.x);
			_n.z += (first.x - sec.x)*(first.y + sec.y);
		}
		
		glm::vec3 n = glm::normalize(_n);
		std::cout << "Normal vector" << n.x << " " << n.y << " " << n.z << std::endl;

	}

	*/
}
void Mesh::DrawWireframe()
{
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	for (int f = 0; f < numFaces; f++)
	{
		glBegin(GL_POLYGON);
		for (int v = 0; v < face[f].nVerts; v++)
		{
			int		iv = face[f].vert[v].vertIndex;

			glVertex3f(pt[iv].x, pt[iv].y, pt[iv].z);
		}
		glEnd();
	}
	
	//draw selected face
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glBegin(GL_POLYGON);
	for (int f = 0; f < numFaces; f++)
	{
		if (face[f].isSelected) {
			for (int v = 0; v < face[f].nVerts; v++)
			{

				int		iv = face[f].vert[v].vertIndex;
				int		ic = face[f].vert[v].colorIndex;

				//ic = f % COLORNUM;

				glColor3f(ColorArr[ic][0], ColorArr[ic][1], ColorArr[ic][2]);
				glVertex3f(pt[iv].x, pt[iv].y, pt[iv].z);
			}

		}
		
	
	}
	glEnd();


 
	
}
void setupMaterial(float ambient[], float diffuse[], float specular[], float shiness)

{

	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambient);

	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuse);

	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular);

	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shiness);

}
void Mesh::DrawColor(int color)
{
	int ic = color;
	GLfloat lightDiffuse[] = { ColorArr[ic][0], ColorArr[ic][1], ColorArr[ic][2], 1.0f };

	GLfloat lightSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };

	GLfloat lightAmbient[] = { 0.4f, 0.4f, 0.4f, 1.0f };

	//GLfloat light_position1[] = { 6.0f, 6.0f, 6.0f, 0.0f };

	setupMaterial(lightAmbient, lightDiffuse, lightSpecular, 100);

	for (int f = 0; f < numFaces; f++) {

		glBegin(GL_POLYGON);

		for (int v = 0; v < face[f].nVerts; v++) {
			int iv = face[f].vert[v].vertIndex;

			glNormal3f(face[f].facenorm.x, face[f].facenorm.y, face[f].facenorm.z);
			glColor3f(ColorArr[ic][0], ColorArr[ic][1], ColorArr[ic][2]);
			glVertex3f(pt[iv].x, pt[iv].y, pt[iv].z);

		}

		glEnd();

	}
}
void Mesh::DrawColor()
{
	int ic = numFaces % 4;
	GLfloat lightDiffuse[] = { ColorArr[ic][0], ColorArr[ic][1], ColorArr[ic][2], 1.0f };

	GLfloat lightSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };

	GLfloat lightAmbient[] = { 0.4f, 0.4f, 0.4f, 1.0f };

	//GLfloat light_position1[] = { 6.0f, 6.0f, 6.0f, 0.0f };

	setupMaterial(lightAmbient, lightDiffuse, lightSpecular, 100);

	for (int f = 0; f < numFaces; f++) {

		glBegin(GL_POLYGON);

		for (int v = 0; v < face[f].nVerts; v++) {
			int iv = face[f].vert[v].vertIndex;

			glNormal3f(face[f].facenorm.x, face[f].facenorm.y, face[f].facenorm.z);
			glColor3f(ColorArr[ic][0], ColorArr[ic][1], ColorArr[ic][2]);
			glVertex3f(pt[iv].x, pt[iv].y, pt[iv].z);

		}

		glEnd();

	}
}
void Mesh::Sym( glm::vec3* pt, int size, int surface) {
	switch (surface) {
	case OYZ:
		for (int i = 0; i<size; i++) {
			pt[i + size]= glm::vec3(-pt[i].x, pt[i].y, pt[i].z);
		}
		break;
	case OXZ:
		for (int i = 0; i<size; i++) {
			pt[i + size]= glm::vec3(pt[i].x, -pt[i].y, pt[i].z);
		}
		break;
	default:
		for (int i = 0; i<size; i++) {
			pt[i + size]= glm::vec3(pt[i].x, pt[i].y, -pt[i].z);
		}
	}
}

void Mesh::SetUpFace(Face& face, int nVerts, int* index, int color) {
	face.nVerts = nVerts;
	face.vert = new VertexID[face.nVerts];
	for (int i = 0; i<nVerts; i++) {
		face.vert[i].vertIndex = index[i];
		face.vert[i].colorIndex = color % 14;
	}
}

int* Mesh::SymNumOYZ(int* index, int num) {
	int* newIndex = new int[4];
	for (int i = 0; i<4; i++) {
		newIndex[i] = index[i] + num;
	}
	return newIndex;
}

int* Mesh::SymNumOXY(int* index, int num) {
	int* newIndex = new int[4];
	for (int i = 0; i<4; i++) {
		newIndex[i] = index[i] + 2 * (num - 1);
	}
	return newIndex;
}


void Mesh::addAllPoint() {
	box = new AABB3();
	box->empty();
	for (int i = 0; i < numVerts; i++) {
		box->add(pt[i]);
	}

	
}

void Mesh::drawAABBBox() {
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glm::vec3 minBox = box->tempMin;
	glm::vec3 maxBox = box->tempMax;
	glm::vec3 boxVertex[] = { glm::vec3(minBox.x,minBox.y,minBox.z),
		glm::vec3(minBox.x,minBox.y,maxBox.z),
		glm::vec3(maxBox.x,minBox.y,maxBox.z),
		glm::vec3(maxBox.x,minBox.y,minBox.z),
		glm::vec3(minBox.x,maxBox.y,minBox.z),
		glm::vec3(minBox.x,maxBox.y,maxBox.z),
		glm::vec3(maxBox.x,maxBox.y,maxBox.z),
		glm::vec3(maxBox.x,maxBox.y,minBox.z),
	};
		glBegin(GL_POLYGON);
		for (int v = 0; v < 8; v++)
		{
			glColor3f(0.5, 0.5, 0.5);
			glVertex3f(boxVertex[v].x, boxVertex[v].y, boxVertex[v].z);
		}
		glEnd();
	

}



void Mesh::CreateXAxis(int nSlices, float height, float radius) {
	numVerts = 42;

	pt = new glm::vec3[numVerts];
	pt[40] = glm::vec3(height, 0.0,  0.0);
	pt[41] = glm::vec3(0.0, 0.0,  0.0);

	float w = 360.0 / 20;

	for (int i = 0; i < 20; i++)
	{
		pt[i] = glm::vec3( height, radius*sin(i * w * 2 * PI / 360), radius*cos(i * w * 2 * PI / 360));
		pt[20 + i] = glm::vec3( 0.0, radius*sin(i * w * 2 * PI / 360), radius*cos(i * w * 2 * PI / 360));
	}

	// ve mat day
	numFaces = 60;
	face = new Face[numFaces];

	// 2 day
	for (int i = 0; i < 19; i++)
	{
		face[i].nVerts = 3;
		face[i].vert = new VertexID[face[i].nVerts];
		face[i].vert[0].vertIndex = 40;
		face[i].vert[1].vertIndex = i + 1;
		face[i].vert[2].vertIndex = i;


		face[19 + i].nVerts = 3;
		face[19 + i].vert = new VertexID[face[i].nVerts];
		face[19 + i].vert[0].vertIndex = 41;
		face[19 + i].vert[1].vertIndex = 20 + i;
		face[19 + i].vert[2].vertIndex = 20 + i + 1;


	}
	{
		face[38].nVerts = 3;
		face[38].vert = new VertexID[face[38].nVerts];
		face[38].vert[0].vertIndex = 40;
		face[38].vert[1].vertIndex = 1;
		face[38].vert[2].vertIndex = 19;



		face[39].nVerts = 3;
		face[39].vert = new VertexID[face[39].nVerts];
		face[39].vert[0].vertIndex = 41;
		face[39].vert[1].vertIndex = 39;
		face[39].vert[2].vertIndex = 20;


	}

	for (int i = 0; i < 19; i++)
	{
		face[40 + i].nVerts = 4;
		face[40 + i].vert = new VertexID[face[40 + i].nVerts];
		face[40 + i].vert[0].vertIndex = i;
		face[40 + i].vert[1].vertIndex = i + 1;
		face[40 + i].vert[2].vertIndex = 20 + i + 1;
		face[40 + i].vert[3].vertIndex = 20 + i;

	}
	face[59].nVerts = 4;
	face[59].vert = new VertexID[face[38].nVerts];
	face[59].vert[0].vertIndex = 19;
	face[59].vert[1].vertIndex = 1;
	face[59].vert[2].vertIndex = 20;
	face[59].vert[3].vertIndex = 39;

	CalculateFacesNorm();
	addAllPoint();
	box->calculateCenter();
	box->calcuateDistance();
}


void Mesh::CreateYAxis(int nSlices, float height, float radius) {
	numVerts = 42;

	pt = new glm::vec3[numVerts];
	pt[40] = glm::vec3(0.0, height, 0.0);
	pt[41] = glm::vec3(0.0, 0.0, 0.0);

	float w = 360.0 / 20;

	for (int i = 0; i < 20; i++)
	{
		pt[i] = glm::vec3(radius*cos(i * w * 2 * PI / 360), height, radius*sin(i * w * 2 * PI / 360));
		pt[20 + i] = glm::vec3(radius*cos(i * w * 2 * PI / 360), 0.0, radius*sin(i * w * 2 * PI / 360));
	}

	// ve mat day
	numFaces = 60;
	face = new Face[numFaces];

	// 2 day
	for (int i = 0; i < 19; i++)
	{
		face[i].nVerts = 3;
		face[i].vert = new VertexID[face[i].nVerts];
		face[i].vert[0].vertIndex = 40;
		face[i].vert[1].vertIndex = i + 1;
		face[i].vert[2].vertIndex = i;


		face[19 + i].nVerts = 3;
		face[19 + i].vert = new VertexID[face[i].nVerts];
		face[19 + i].vert[0].vertIndex = 41;
		face[19 + i].vert[1].vertIndex = 20 + i;
		face[19 + i].vert[2].vertIndex = 20 + i + 1;


	}
	{
		face[38].nVerts = 3;
		face[38].vert = new VertexID[face[38].nVerts];
		face[38].vert[0].vertIndex = 40;
		face[38].vert[1].vertIndex = 1;
		face[38].vert[2].vertIndex = 19;



		face[39].nVerts = 3;
		face[39].vert = new VertexID[face[39].nVerts];
		face[39].vert[0].vertIndex = 41;
		face[39].vert[1].vertIndex = 39;
		face[39].vert[2].vertIndex = 20;


	}

	for (int i = 0; i < 19; i++)
	{
		face[40 + i].nVerts = 4;
		face[40 + i].vert = new VertexID[face[40 + i].nVerts];
		face[40 + i].vert[0].vertIndex = i;
		face[40 + i].vert[1].vertIndex = i + 1;
		face[40 + i].vert[2].vertIndex = 20 + i + 1;
		face[40 + i].vert[3].vertIndex = 20 + i;

	}
	face[59].nVerts = 4;
	face[59].vert = new VertexID[face[38].nVerts];
	face[59].vert[0].vertIndex = 19;
	face[59].vert[1].vertIndex = 1;
	face[59].vert[2].vertIndex = 20;
	face[59].vert[3].vertIndex = 39;

	CalculateFacesNorm();
	addAllPoint();
	box->calculateCenter();
	box->calcuateDistance();
}

void Mesh::CreateZAxis(int nSlices, float height, float radius) {
	numVerts = 42;

	pt = new glm::vec3[numVerts];
	pt[40] = glm::vec3(0.0, 0.0, height);
	pt[41] = glm::vec3(0.0, 0.0, 0.0);

	float w = 360.0 / 20;

	for (int i = 0; i < 20; i++)
	{
		pt[i] = glm::vec3(radius*sin(i * w * 2 * PI / 360),radius*cos(i * w * 2 * PI / 360), height);
		pt[20 + i] = glm::vec3(radius*sin(i * w * 2 * PI / 360),radius*cos(i * w * 2 * PI / 360), 0.0);
	}

	// ve mat day
	numFaces = 60;
	face = new Face[numFaces];

	// 2 day
	for (int i = 0; i < 19; i++)
	{
		face[i].nVerts = 3;
		face[i].vert = new VertexID[face[i].nVerts];
		face[i].vert[0].vertIndex = 40;
		face[i].vert[1].vertIndex = i + 1;
		face[i].vert[2].vertIndex = i;


		face[19 + i].nVerts = 3;
		face[19 + i].vert = new VertexID[face[i].nVerts];
		face[19 + i].vert[0].vertIndex = 41;
		face[19 + i].vert[1].vertIndex = 20 + i;
		face[19 + i].vert[2].vertIndex = 20 + i + 1;


	}
	{
		face[38].nVerts = 3;
		face[38].vert = new VertexID[face[38].nVerts];
		face[38].vert[0].vertIndex = 40;
		face[38].vert[1].vertIndex = 1;
		face[38].vert[2].vertIndex = 19;



		face[39].nVerts = 3;
		face[39].vert = new VertexID[face[39].nVerts];
		face[39].vert[0].vertIndex = 41;
		face[39].vert[1].vertIndex = 39;
		face[39].vert[2].vertIndex = 20;


	}

	for (int i = 0; i < 19; i++)
	{
		face[40 + i].nVerts = 4;
		face[40 + i].vert = new VertexID[face[40 + i].nVerts];
		face[40 + i].vert[0].vertIndex = i;
		face[40 + i].vert[1].vertIndex = i + 1;
		face[40 + i].vert[2].vertIndex = 20 + i + 1;
		face[40 + i].vert[3].vertIndex = 20 + i;

	}
	face[59].nVerts = 4;
	face[59].vert = new VertexID[face[38].nVerts];
	face[59].vert[0].vertIndex = 19;
	face[59].vert[1].vertIndex = 1;
	face[59].vert[2].vertIndex = 20;
	face[59].vert[3].vertIndex = 39;

	CalculateFacesNorm();
	addAllPoint();
	box->calculateCenter();
	box->calcuateDistance();
}




void Mesh::CalculateFacesNorm() {
	int nVerts = 0;
	for (int i = 0; i < numFaces; i++) {
		face[i].facenorm.x = face[i].facenorm.y = face[i].facenorm.z = 0;
		nVerts = face[i].nVerts;
		for (int j = 0; j < nVerts; j++) {
			face[i].facenorm.x += (pt[face[i].vert[j].vertIndex].y - pt[face[i].vert[(j + 1) % nVerts].vertIndex].y)*(pt[face[i].vert[j].vertIndex].z + pt[face[i].vert[(j + 1) % nVerts].vertIndex].z);
			face[i].facenorm.y += (pt[face[i].vert[j].vertIndex].z - pt[face[i].vert[(j + 1) % nVerts].vertIndex].z)*(pt[face[i].vert[j].vertIndex].x + pt[face[i].vert[(j + 1) % nVerts].vertIndex].x);
			face[i].facenorm.z += (pt[face[i].vert[j].vertIndex].x - pt[face[i].vert[(j + 1) % nVerts].vertIndex].x)*(pt[face[i].vert[j].vertIndex].y + pt[face[i].vert[(j + 1) % nVerts].vertIndex].y);
		}
		face[i].facenorm = glm::normalize(face[i].facenorm);
	}
}

bool Mesh::RayABBBIntersect(glm::vec3& o, glm::vec3& d, float& value) {
	return box->RayABBBIntersect(o, d, value);

}

void Mesh::setToTransformedBox(const glm::mat4x3 &m) {
	
	
	box->setToTransformedBox(m);
	box->calculateCenter();
	box->calcuateDistance();
}

void Mesh::EmptyAABB() {
	box->empty();
};

float Mesh::distanceToCenter() {
	
	return box->center.length();
}

float Mesh::distance(glm::vec3& to) {
	return length(box->center - to);
}

glm::vec3 calculateFaceCenter(Face* face,Mesh* mesh)
{

	glm::vec3 faceCenter = glm::vec3(0);
	for (int i = 0; i < face->nVerts; i++) {
		faceCenter[0] += mesh->pt[face->vert[i].vertIndex].x;
		faceCenter[1] += mesh->pt[face->vert[i].vertIndex].y;
		faceCenter[2] += mesh->pt[face->vert[i].vertIndex].z;
	}
	return faceCenter;
}
