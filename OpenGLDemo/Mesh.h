#pragma once

#include "glm\glm.hpp"
#include <glut.h>
#include <iostream>
 
class AABB3;



class VertexID
{
public:
	int		vertIndex;
	int		colorIndex;
};

class Face
{
public:
	glm::vec3 facenorm;
	bool isSelected = false;
	int		nVerts;
	VertexID*	vert;

	Face()
	{
		nVerts = 0;
		vert = NULL;
	}
	~Face()
	{
		if (vert != NULL)
		{
			delete[] vert;
			vert = NULL;
		}
		nVerts = 0;
	}
};

class Mesh
{
public:


	bool isSelected = false;
	int		numVerts;
	glm::vec3*		pt;
	int colorIndex;
	AABB3* box = nullptr;
	int		numFaces;
	Face*		face;
	float slideX, slideY, slideZ;
	float rotateX, rotateY, rotateZ;
	float scaleX, scaleY, scaleZ;
	glm::mat4 transMatrix;
	friend glm::vec3 calculateFaceCenter(Face* face,Mesh* mesh);
public:
	Mesh()
	{
		transMatrix = glm::mat4(1);
		numVerts = 0;
		pt = NULL;
		numFaces = 0;
		face = NULL;
	}
	~Mesh()
	{
		if (pt != NULL)
		{
			delete[] pt;
		}
		if (face != NULL)
		{
			delete[] face;
		}
		numVerts = 0;
		numFaces = 0;
	}
	void tick();
	void DrawWireframe();
	void DrawColor(int color);
	void DrawColor();
	void EmptyAABB();
	void CreateCube(float	fSize);


	void Sym(glm::vec3* pt, int size, int plane);
	void SetUpFace(Face& face, int nVerts, int* index, int color);
	int* SymNumOYZ(int* index, int num);
	int* SymNumOXY(int* index, int num);
	void SetColor(int colorIdx);
	void CreateXAxis(int nSlices, float h, float r);
	void CreateYAxis(int nSlices, float h, float r);
	void CreateZAxis(int nSlices, float h, float r);

	// add all point to box
	void addAllPoint();
	// draw AABB box of this Mesh
	void drawAABBBox();

	void CreateUVSphere(float radius);

	void CreateTorus(float radius, float thickness);

	void CreateCube(float fSizeX, float fSizeY, float fSizeZ);

	void CreateCone(float radius, float height);

	bool RayABBBIntersect(glm::vec3& o, glm::vec3& d, float& value);

	void setToTransformedBox(const glm::mat4x3 &m);

	// Calculate from center of the AABB box
	float distanceToCenter();
	float distance(glm::vec3& to);

	// calculate normal vector
	void CalculateFacesNorm();

	void CreateCylinder(float radius, float height);

};

