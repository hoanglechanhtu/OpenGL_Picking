#pragma once
#include"Mesh.h"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

template <class T>
struct Node {
	T* value;
	Node<T>* next = nullptr;

};


template <class T>
class List {
public:

	Node<T>* head =  nullptr;
	Node<T>* CreateNewMesh() {
		if (head == nullptr) {
			head = new Node<T>();
			head->next = nullptr;
			head->value = new T();
			return head;
		}
		else {
			Node<T>* temp = head;
			while (temp->next != nullptr) {
				temp = temp->next;
			}
			temp->next = new Node<T>();
			temp->next->value = new T();
			return temp->next;
		}
	}
};

class MeshManager
{
public:
	MeshManager();
	~MeshManager();
	static Mesh *xAxis;
	static Mesh *yAxis;
	static Mesh *zAxis;
	static List<Mesh>* list;
 
	static Mesh* selectedMesh;
	static Face* selectedFace;
	static void CreateCylinder();
 
	static float	baseRadius;
	static float	baseHeight;
	static int globalColor;
	static void CreateAxis();
	static void CreateTorus();

	static void CreateCube();
	static Node<Mesh>* getHeadOfVertex();

	static void CreateSphere();

	static void CreateCone();
};

