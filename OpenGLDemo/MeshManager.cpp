#include "MeshManager.h"

Mesh* MeshManager::xAxis = nullptr;
Mesh* MeshManager::yAxis = nullptr;
Mesh* MeshManager::zAxis = nullptr;
 
Face*  MeshManager::selectedFace = nullptr;
Mesh* MeshManager::selectedMesh = nullptr;
float MeshManager::baseRadius = 1;
float MeshManager::baseHeight = 0.2;
int MeshManager::globalColor = 0;
List<Mesh>* MeshManager::list = new List<Mesh>();
MeshManager::MeshManager()
{

}


MeshManager::~MeshManager()
{
}

void MeshManager::CreateCylinder() {
	Node<Mesh>* newMesh = list->CreateNewMesh();
	newMesh->value->CreateCylinder(0.1, 10 );
	newMesh->value->transMatrix = glm::mat4(glm::translate(glm::mat4(1), glm::vec3(-10, 0, 0)));
	newMesh->value->SetColor(globalColor % 14);
	globalColor++;

}
 
void MeshManager::CreateAxis()
{
	Node<Mesh>* newMeshX = list->CreateNewMesh();
	newMeshX->value->CreateXAxis(20, 10, 0.1);
	newMeshX->value->SetColor(globalColor % 14);
	globalColor++;
	//x
	Node<Mesh>* newMeshY = list->CreateNewMesh();
	newMeshY->value->CreateYAxis(20, 10, 0.1);
	newMeshY->value->SetColor(globalColor % 14);
	globalColor++;// y
	Node<Mesh>* newMeshZ = list->CreateNewMesh();
	newMeshZ->value->CreateZAxis(20, 10, 0.1);
	newMeshZ->value->SetColor(globalColor % 14);
	globalColor++; // z
	xAxis = list->head->value;
	yAxis = list->head->next->value;
	zAxis = list->head->next->next->value;
	/*
	xAxis->transMatrix = glm::rotate(xAxis->transMatrix, glm::radians(-90.0f), glm::vec3(0, 0, 1));
	xAxis->transMatrix = glm::translate(xAxis->transMatrix, glm::vec3(0,5,0));
	xAxis->setToTransformedBox(xAxis->transMatrix);
	zAxis->transMatrix = glm::rotate(zAxis->transMatrix, glm::radians(-90.0f), glm::vec3(1, 0, 0));
	zAxis->transMatrix = glm::translate(zAxis->transMatrix, glm::vec3(0, -5, 0));
	zAxis->setToTransformedBox(zAxis->transMatrix);
	yAxis->transMatrix = glm::translate(yAxis->transMatrix, glm::vec3(0, 5, 0));
	yAxis->setToTransformedBox(zAxis->transMatrix)*/
}

void MeshManager::CreateTorus() {
	Node<Mesh>* newMesh = list->CreateNewMesh();
	newMesh->value->CreateTorus(baseRadius * 3, baseRadius / 2);
	newMesh->value->transMatrix = glm::mat4(glm::translate(glm::mat4(1), glm::vec3(-10, 0, 0)));
	newMesh->value->SetColor(globalColor % 14);
	globalColor++;
}

void MeshManager::CreateCube() {
	Node<Mesh>* newMesh = list->CreateNewMesh();
	newMesh->value->CreateCube(baseRadius);
	newMesh->value->transMatrix = glm::mat4(glm::translate(glm::mat4(1), glm::vec3(-10, 0, 0)));
	newMesh->value->SetColor(globalColor % 14);
	globalColor++;
}

Node<Mesh>* MeshManager::getHeadOfVertex()
{
	if (zAxis != nullptr) {
		 
			return list->head->next->next->next;
	}
	return nullptr;
}

void MeshManager::CreateSphere() {
	Node<Mesh>* newMesh = list->CreateNewMesh();
	newMesh->value->CreateUVSphere(baseRadius);
	newMesh->value->transMatrix = glm::mat4(glm::translate(glm::mat4(1), glm::vec3(-10, 0, 0)));
	newMesh->value->SetColor(globalColor % 14);
	globalColor++;
}

void MeshManager::CreateCone() {
	Node<Mesh>* newMesh = list->CreateNewMesh();
	newMesh->value->CreateCone(baseRadius, baseHeight*10);
	newMesh->value->transMatrix = glm::mat4(glm::translate(glm::mat4(1), glm::vec3(-10, 0, 0)));
	newMesh->value->SetColor(globalColor % 14);
	globalColor++;
}