// Lab3.cpp : Defines the entry point for the console application.
//
#include"Camera.h"
#include"MeshManager.h"
#include "Mesh.h"
#include <math.h>
#include <iostream>
#include <glut.h>
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/matrix_access.hpp"
#include "glm/gtc/type_ptr.hpp"

#include "Lab3.h"

using namespace std;


 
#define PI			3.1415926
#define D2G			(PI/ 180)



//Global camera
Camera globalCamera;

MeshManager meshManager;
 

enum ModifiMode
{
	Translate,
	Rotate,
	Scale,
	Create
};
//my position
enum Mesh_Face
{
	MeshModify,
	FaceModify
};
struct MousePosition {
	float x;
	float y;

};


	MousePosition myMousePosition;
	int		screenWidth = 800;
	int		screenHeight = 600;

	bool	bAABB = false;
	bool	bWireFrame = false;
	bool    b4View = true;
	bool	isLightOn = true;
	
	float	baseRotateStep = 5;

	float	columnSizeX = 0.3;
	float	columnSizeZ = columnSizeX;
	float	columnSizeY = 5;

	Mesh	base;
	Mesh	column;
	Mesh cube;
 



	//camera setup
	 
 


	// delta time tracking

	float deltaTime = 0.0f;	// Time between current frame and last frame
	float lastFrame = 0.0f; // Time of last frame



	//mouse motion track

	bool firstMouse = true;
	float yaw = -90.0f;	// yaw is initialized to -90.0 degrees since a yaw of 0.0 results in a direction vector pointing to the right so we initially rotate a bit to the left.
	float pitch = 0.0f;
	float lastX = 800.0f / 2.0;
	float lastY = 600.0 / 2.0;
	float fov = 45.0f;




	//
	 

	int InputMode = Translate;
	int Mesh_FaceMode = MeshModify;
	void mouseMotion(int x, int y);
	void drawAxis();
	glm::mat4& skipScale(glm::mat4& value);
	void mymouse(int btn, int state, int x, int y);
	void transformFace(glm::mat4& transMatrix);
	 
	bool RaySphereIntersect(glm::vec3 &o, glm::vec3 &d, glm::vec3 &c, float r, float& dis);
 
	bool RayPlaneIntersect(glm::vec3 &o, glm::vec3 &d, Face* checkFace, float& distance);
	bool checkPointInsideRect(glm::vec3 &o, glm::vec3 &x, glm::vec3 &y, glm::vec3 &z);
 
/////
void myKeyboard(unsigned char key, int x, int y)
{
	float cameraSpeed = globalCamera.getCameraSpeed();
	if (key == 'l') isLightOn = !isLightOn;
	if (key == 'v')
		InputMode = Create;
	if (InputMode == Create) {
		switch (key)
		{
		case'f':
			Mesh_FaceMode = (Mesh_FaceMode == MeshModify) ? FaceModify : MeshModify;
			break;

		case 'm':
			globalCamera.returnToDefaultSetting();
			break;
		case 't':
			globalCamera.setFrontView();
			break;
		case 'g':
			globalCamera.setBackView();
			break;
		case 'y':
			globalCamera.setRightView();
			break;
		case 'h':
			globalCamera.setLeftView();
			break;
		case 'u':
			globalCamera.setTopView();
			break;
		case 'j':
			globalCamera.setDownView();
			break;
		case 'k':
			if (MeshManager::selectedMesh != nullptr) delete MeshManager::selectedMesh;
			break;
		case '1':
			bAABB = !bAABB;
			break;
		case'2':
			MeshManager::CreateCylinder();
			break;
		case'3':
			MeshManager::CreateTorus();
			break;
		case'4':
			MeshManager::CreateCube();
			break;
		case'5':
			MeshManager::CreateSphere();
			break;
		case'6':
			MeshManager::CreateCone();
			break;
		case 'w':
			globalCamera.adjustCameraPosition(cameraSpeed*globalCamera.getCameraFront());
			
			break;
		case 's':
			globalCamera.adjustCameraPosition(-cameraSpeed*globalCamera.getCameraFront());
		 
			break;
		case 'a':
			globalCamera.adjustCameraPosition(-glm::normalize(glm::cross(globalCamera.getCameraFront() , globalCamera.getCameraUp() ))*cameraSpeed);
			 
			break;
		case 'd':
			globalCamera.adjustCameraPosition(glm::normalize(glm::cross(globalCamera.getCameraFront(), globalCamera.getCameraUp()))*cameraSpeed);
			 
			break;
		case 'q':
			globalCamera.adjustCameraPosition(cameraSpeed*globalCamera.getCameraUp()); 
			break;
		case 'e':
			globalCamera.adjustCameraPosition(-cameraSpeed*globalCamera.getCameraUp());
			 
			break;
		case'b':
			bWireFrame = !bWireFrame;
			break;
		case 'z':
			InputMode = Translate;
			break;
		case 'x':
			InputMode = Rotate;
			break;
		case 'c':
			InputMode = Scale;
			break;
		case 'v':
			InputMode = Create;
			break;
		}
	}
	else {

		//  create a transMatrix in Mesh class and use it through MeshManager::selectedMesh
	 
		  if (!MeshManager::selectedFace&&MeshManager::selectedMesh) {
			// input for mesh transform
			if (InputMode == Translate) {
				float BaseTransformRate = 0.2;
				switch (key)
				{
				case '1':
					bAABB = !bAABB;
					break;
				case 'w':
					
					MeshManager::selectedMesh->transMatrix = glm::translate(MeshManager::selectedMesh->transMatrix, BaseTransformRate* globalCamera.getCameraFront());
					break;
				case 's':
					MeshManager::selectedMesh->transMatrix = glm::translate(MeshManager::selectedMesh->transMatrix, -BaseTransformRate* globalCamera.getCameraFront());
					break;
				case 'a':
					//TODO remove hard codes
					//MeshManager::selectedMesh->transMatrix = glm::translate(MeshManager::selectedMesh->transMatrix, glm::vec3(1,0,0));
					MeshManager::selectedMesh->transMatrix = glm::translate(MeshManager::selectedMesh->transMatrix, BaseTransformRate*glm::normalize(glm::cross(globalCamera.getCameraUp(), globalCamera.getCameraFront())));
					break;
				case 'd':
					MeshManager::selectedMesh->transMatrix = glm::translate(MeshManager::selectedMesh->transMatrix, BaseTransformRate *-glm::normalize(glm::cross(globalCamera.getCameraUp(), globalCamera.getCameraFront())));
					break;
				case 'q':
					MeshManager::selectedMesh->transMatrix = glm::translate(MeshManager::selectedMesh->transMatrix, BaseTransformRate* globalCamera.getCameraUp());
					break;
				case 'e':
					MeshManager::selectedMesh->transMatrix = glm::translate(MeshManager::selectedMesh->transMatrix, BaseTransformRate *-globalCamera.getCameraUp());
					break;
				case 'z':
					InputMode = Translate;
					break;
				case 'x':
					InputMode = Rotate;
					break;
				case 'c':
					InputMode = Scale;
					break;
				case 'v':
					InputMode = Create;
					break;
				}
			}
			else if (InputMode == Rotate) {
				// base rotation rate
				float baseRotationDegree = 5;
				switch (key)
				{


				case '1':
					bAABB = !bAABB;
					break;
				case 'w':
					MeshManager::selectedMesh->transMatrix = glm::rotate(MeshManager::selectedMesh->transMatrix, glm::radians(baseRotationDegree), glm::normalize(glm::cross(globalCamera.getCameraUp(), globalCamera.getCameraFront())));
					break;
				case 's':
					MeshManager::selectedMesh->transMatrix = glm::rotate(MeshManager::selectedMesh->transMatrix, glm::radians(baseRotationDegree), -glm::normalize(glm::cross(globalCamera.getCameraUp(), globalCamera.getCameraFront())));
					break;
				case 'a':
					MeshManager::selectedMesh->transMatrix = glm::rotate(MeshManager::selectedMesh->transMatrix, glm::radians(baseRotationDegree), globalCamera.getCameraUp());
					break;
				case 'd':
					MeshManager::selectedMesh->transMatrix = glm::rotate(MeshManager::selectedMesh->transMatrix, glm::radians(baseRotationDegree), -globalCamera.getCameraUp());
					break;
				case 'q':
					MeshManager::selectedMesh->transMatrix = glm::rotate(MeshManager::selectedMesh->transMatrix, glm::radians(baseRotationDegree), globalCamera.getCameraFront());
					break;
				case 'e':
					MeshManager::selectedMesh->transMatrix = glm::rotate(MeshManager::selectedMesh->transMatrix, glm::radians(baseRotationDegree), -globalCamera.getCameraFront());
					break;
				case 'z':
					InputMode = Translate;
					break;
				case 'x':
					InputMode = Rotate;
					break;
				case 'c':
					InputMode = Scale;
					break;
				case 'v':
					InputMode = Create;
					break;
				}

			}

			else if (InputMode == Scale) {
				// base rotation rate
				float basePositiveScaleRate = 1.1;
				float baseNegativeScaleRate = 0.9;
				switch (key)
				{


				case '1':
					bAABB = !bAABB;
					break;
				case 'w':
					MeshManager::selectedMesh->transMatrix = glm::scale(MeshManager::selectedMesh->transMatrix, glm::vec3(1, basePositiveScaleRate, 1));
					break;
				case 's':
					MeshManager::selectedMesh->transMatrix = glm::scale(MeshManager::selectedMesh->transMatrix, glm::vec3(1, baseNegativeScaleRate, 1));
					break;
				case 'a':
					MeshManager::selectedMesh->transMatrix = glm::scale(MeshManager::selectedMesh->transMatrix, glm::vec3(basePositiveScaleRate, 1, 1));
					break;
				case 'd':
					MeshManager::selectedMesh->transMatrix = glm::scale(MeshManager::selectedMesh->transMatrix, glm::vec3(baseNegativeScaleRate, 1, 1));
					break;
				case 'q':
					MeshManager::selectedMesh->transMatrix = glm::scale(MeshManager::selectedMesh->transMatrix, glm::vec3(1, 1, basePositiveScaleRate));
					break;
				case 'e':
					MeshManager::selectedMesh->transMatrix = glm::scale(MeshManager::selectedMesh->transMatrix, glm::vec3(1, 1, baseNegativeScaleRate));
					break;
				case 'z':
					InputMode = Translate;
					break;
				case 'x':
					InputMode = Rotate;
					break;
				case 'c':
					InputMode = Scale;
					break;
				case 'v':
					InputMode = Create;
					break;
				}

			}
		}
		else if(MeshManager::selectedFace) {
			//input for face transform
			glm::mat4 transMatrix = glm::mat4(1);
			if (InputMode == Translate) {
				float BaseTransformRate = 0.2;
				switch (key)
				{
				case '1':
					bAABB = !bAABB;
					break;
				case 'w':
					transMatrix = glm::translate(transMatrix, BaseTransformRate* globalCamera.getCameraFront());
					transformFace(transMatrix);
					break;
				case 's':
					transMatrix = glm::translate(transMatrix, -BaseTransformRate*globalCamera.getCameraFront());
					transformFace(transMatrix);
					 
					break;
				case 'a':
					transMatrix = glm::translate(transMatrix, BaseTransformRate*glm::normalize(glm::cross(globalCamera.getCameraUp(), globalCamera.getCameraFront())));
					transformFace(transMatrix);
					 
					break;
				case 'd':
					 
					transMatrix = glm::translate(transMatrix, BaseTransformRate *-glm::normalize(glm::cross(globalCamera.getCameraUp(), globalCamera.getCameraFront())));
					transformFace(transMatrix);
					break;
				case 'q':
					 
					transMatrix = glm::translate(transMatrix, BaseTransformRate* globalCamera.getCameraUp());
					transformFace(transMatrix);
					break;
				case 'e':
				 
					transMatrix = glm::translate(transMatrix, BaseTransformRate *-globalCamera.getCameraUp());
					transformFace(transMatrix);

					break;
				case 'z':
					InputMode = Translate;
					break;
				case 'x':
					InputMode = Rotate;
					break;
				case 'c':
					InputMode = Scale;
					break;
				case 'v':
					InputMode = Create;
					break;
				}
			}
			else if (InputMode == Rotate) {
				// base rotation rate
				float baseRotationDegree = 5;
				switch (key)
				{
				case '1':
					bAABB = !bAABB;
					break;
				case 'w':
					transMatrix = glm::rotate(transMatrix, glm::radians(baseRotationDegree), glm::normalize(glm::cross(globalCamera.getCameraUp(), globalCamera.getCameraFront())));
					transformFace(transMatrix);
					break;
				case 's':
					transMatrix = glm::rotate(transMatrix, glm::radians(baseRotationDegree), -glm::normalize(glm::cross(globalCamera.getCameraUp(), globalCamera.getCameraFront())));
					transformFace(transMatrix);

					break;
				case 'a':
					transMatrix = glm::rotate(transMatrix, glm::radians(baseRotationDegree), globalCamera.getCameraUp());
					transformFace(transMatrix);

					break;
				case 'd':

					transMatrix = glm::rotate(transMatrix, glm::radians(baseRotationDegree), -globalCamera.getCameraUp());
					transformFace(transMatrix);
					break;
				case 'q':

					transMatrix = glm::rotate(transMatrix, glm::radians(baseRotationDegree), globalCamera.getCameraFront());
					transformFace(transMatrix);
					break;
				case 'e':

					transMatrix = glm::rotate(transMatrix, glm::radians(baseRotationDegree), -globalCamera.getCameraFront());
					transformFace(transMatrix);

					break;
				case 'z':
					InputMode = Translate;
					break;
				case 'x':
					InputMode = Rotate;
					break;
				case 'c':
					InputMode = Scale;
					break;
				case 'v':
					InputMode = Create;
					break;
				}
			 

			}

			else if (InputMode == Scale) {
				// base rotation rate
				float basePositiveScaleRate = 1.1;
				float baseNegativeScaleRate = 0.9;

				switch (key)
				{
				case '1':
					bAABB = !bAABB;
					break;
				case 'w':
					transMatrix = glm::scale(transMatrix, glm::vec3(1, basePositiveScaleRate, 1));
					transformFace(transMatrix);
					break;
				case 's':
					transMatrix = glm::scale(transMatrix, glm::vec3(1, baseNegativeScaleRate, 1));
					transformFace(transMatrix);

					break;
				case 'a':
					transMatrix = glm::scale(transMatrix, glm::vec3(basePositiveScaleRate, 1, 1));
					transformFace(transMatrix);

					break;
				case 'd':

					transMatrix = glm::scale(transMatrix, glm::vec3(baseNegativeScaleRate, 1, 1));
					transformFace(transMatrix);
					break;
				case 'q':

					transMatrix = glm::scale(transMatrix, glm::vec3(1, 1, basePositiveScaleRate));
					transformFace(transMatrix);
					break;
				case 'e':

					transMatrix = glm::scale(transMatrix, glm::vec3(1, 1, baseNegativeScaleRate));
					transformFace(transMatrix);

					break;
				case 'z':
					InputMode = Translate;
					break;
				case 'x':
					InputMode = Rotate;
					break;
				case 'c':
					InputMode = Scale;
					break;
				case 'v':
					InputMode = Create;
					break;
				}

			 
			}
		}
	}
	 
	 
    glutPostRedisplay();
}
void drawAxis()
{
	glPushMatrix();
	glm::mat4 meshTransMatrix = glm::mat4(1);
	glm::mat4 faceTransMatrix;
	if (MeshManager::selectedMesh != nullptr) {
		if (MeshManager::selectedFace != nullptr) {
			glm::vec3 facePosition = calculateFaceCenter(MeshManager::selectedFace, MeshManager::selectedMesh);
			glm::vec3 faceNorm = MeshManager::selectedFace->facenorm;
			//faceTransMatrix = glm::mat4(glm::vec4(faceNorm,facePosition.x),
		}
	
	meshTransMatrix = MeshManager::selectedMesh->transMatrix;
	
	glm::mat4x3 transBox = glm::mat4x3(MeshManager::selectedMesh->transMatrix);
	 
	 
	MeshManager::xAxis->setToTransformedBox(transBox);
	MeshManager::yAxis->setToTransformedBox(transBox);
	MeshManager::zAxis->setToTransformedBox(transBox);
 
	//glm::mat4 transpose = glm::transpose(transMatrix);
	meshTransMatrix =   (MeshManager::selectedMesh->transMatrix);
	//glTranslated(2, 0, 2);
	 
	}
	float* temp = new float[16];
	glm::mat4 axisXTransMatrix = meshTransMatrix*MeshManager::xAxis->transMatrix;
	temp = glm::value_ptr(skipScale(axisXTransMatrix));
	glMultMatrixf(temp);
	MeshManager::xAxis->DrawColor(1);
	glPopMatrix();
	glPushMatrix();
	glm::mat4 axisYTransMatrix = meshTransMatrix*MeshManager::yAxis->transMatrix;
	temp = glm::value_ptr(skipScale(axisYTransMatrix));
	glMultMatrixf(temp);
	MeshManager::yAxis->DrawColor(2);
	glPopMatrix();
	glPushMatrix();
	glm::mat4 axisZTransMatrix = meshTransMatrix*MeshManager::zAxis->transMatrix;
	temp = glm::value_ptr(skipScale(axisZTransMatrix));
	glMultMatrixf(temp);
	MeshManager::zAxis->DrawColor(3);
	glPopMatrix();
	glLineWidth(1);
}

glm::mat4& skipScale(glm::mat4& value)
{

	float x = sqrt(value[0][0] * value[0][0] + value[0][1] * value[0][1] + value[0][2] * value[0][2]);
	if (x != 0) {
		value[0][0] = value[0][0]/x;
		value[0][1] = value[0][1] / x;
		value[0][2] = value[0][2] / x;

	}

	float y = sqrt(value[1][0] * value[1][0] + value[1][1] * value[1][1] + value[1][2] * value[1][2]);
	if (y != 0) {
		value[1][0] = value[1][0] / y;
		value[1][1] = value[1][1] / y;
		value[1][2] = value[1][2] / y;

	}

	float z = sqrt(value[2][0] * value[2][0] + value[2][1] * value[2][1] + value[2][2] * value[2][2]);
	if (z != 0) {
		value[2][0] = value[2][0] / z;
		value[2][1] = value[2][1] / z;
		value[2][2] = value[2][2] / z;

	}
	 
	return value;
}


void drawMesh(Mesh* mesh) {
	glPushMatrix();
	glm::mat4x3 transBox = glm::mat4x3(mesh->transMatrix);

	mesh->setToTransformedBox(transBox);


	//glTranslated(0,2, 0);
	//glRotated(45, 0, 0, 1);
	//glRotatef(base.rotateY, 0, 1, 0);


	float* temp = new float[16];
	//glm::mat4 transpose = glm::transpose(transMatrix);
	temp = glm::value_ptr(mesh->transMatrix);
	//glTranslated(2, 0, 2);
	glMultMatrixf(temp);

	if (bWireFrame || mesh->isSelected)
		mesh->DrawWireframe();
	else
		mesh->DrawColor();

	glPopMatrix();

	//draw cube tria
} 
 void myDisplay(){

	 glMatrixMode(GL_PROJECTION);
	 glLoadIdentity();
	// if(b4View)
     glFrustum(-10.0, 10.0, -10.0, 10.0, 2.5f, 2000.0);
	 //else glOrtho(-10.0f, 10.0f, -10.0f, 10.0f, 0.1f, 100.0f);
	 //gluPerspective(glm::radians(45.0f), (float)screenWidth / (float)screenHeight, -1.0f, 100.0f);
 
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	 
	
	gluLookAt(globalCamera.getCameraPosition().x, 
		globalCamera.getCameraPosition().y, 
		globalCamera.getCameraPosition().z, 
		(globalCamera.getCameraPosition() + globalCamera.getCameraFront()).x,
		(globalCamera.getCameraPosition() + globalCamera.getCameraFront()).y,
		(globalCamera.getCameraPosition() + globalCamera.getCameraFront()).z,
		globalCamera.getCameraUp().x,
		globalCamera.getCameraUp().y, 
		globalCamera.getCameraUp().z);
 
	


	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glViewport(0, 0, screenWidth, screenHeight);
	
	//


	 
	 
	//
	// lighting
	GLfloat lightDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };

	GLfloat lightSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };

	GLfloat lightAmbient[] = { 0.4f, 0.4f, 0.4f, 1.0f };

	GLfloat light_position1[] = { 100.0f, 100.0f, 100.0f, 0.0f };


	glLightfv(GL_LIGHT0, GL_POSITION, light_position1);

	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);

	glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);

	glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular);

	if (isLightOn) {
		glEnable(GL_LIGHTING);

		glEnable(GL_LIGHT0);
	}
	else {
		glEnable(GL_LIGHTING);

		glEnable(GL_LIGHT0);
		glDisable(GL_LIGHTING);
	}

	drawAxis();

	
 
	if (bAABB) {
		Node<Mesh>* tempMesh = MeshManager::getHeadOfVertex();
		while (tempMesh) {
			tempMesh->value->drawAABBBox();
			tempMesh = tempMesh->next;
		}
		//
		 
	}
	else {
		Node<Mesh>* tempMesh = MeshManager::getHeadOfVertex();
		while (tempMesh) {
			drawMesh(tempMesh->value);
			tempMesh = tempMesh->next;

		}
	}
	//MeshManager::list->head->value->tick();
	glFlush();
    glutSwapBuffers();
}

 void myInit()
 {
	 float	fHalfSize = 4;

	 glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

	// glutSetInputMode
	 glFrontFace(GL_CCW);
	 //glEnable(GL_DEPTH_TEST);

	 glMatrixMode(GL_PROJECTION);
	 glLoadIdentity();
	// glOrtho(-10.0f, 10.0f, -10.0f, 10.0f, 0.1f, 100.0f);
	 //gluPerspective(glm::radians(45.0f), (float)screenWidth / (float)screenHeight, 10.0f, 100.0f);
	 //glFrustum(-10.0, 10.0, -10.0, 10.0, 2.5f, 20.0);
	 glMatrixMode(GL_MODELVIEW);
	 glLoadIdentity();
 }




int main(int argc, char* argv[])
{
	glutInit(&argc, (char**)argv); //initialize the tool kit
	glutInitDisplayMode(GLUT_DOUBLE |GLUT_RGB | GLUT_DEPTH);//set the display mode
	glutInitWindowSize(screenWidth, screenHeight); //set window size
	glutInitWindowPosition(100, 100); // set window position on screen
	glutCreateWindow("Lab 3 - Demo (2017-2018)"); // open the screen window
	glEnable(GL_DEPTH_TEST);

	InputMode = Create;

 
	
	MeshManager::CreateAxis();
	MeshManager::CreateCube();
 
 

	//check selected face
	

	myInit();
	
	
	glutMouseFunc(mymouse);
	
	glutMotionFunc(mouseMotion);

	glutKeyboardFunc(myKeyboard);
	
    glutDisplayFunc(myDisplay);

	glutMainLoop();
	return 0;
}

void mymouse(int btn, int state, int mouse_x, int mouse_y) {
	if (btn == 1)
	{
		bWireFrame = true;
	}
	 
	if (btn == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
	{
		//mouse_x = screenHeight / 2;
		//mouse_y = screenWidth / 2;
		////cout << mouse_x << "    " << mouse_y << endl;
		 
		mouse_y = screenHeight - mouse_y;
		//transform it into 3d normalised device coordinate
		float x = (2.0f * mouse_x) / screenWidth - 1.0f;
		float y = (2.0f * mouse_y) / screenHeight - 1.0f;

		////cout << x << "    " << y << endl;
		float z = 1.0f;
		//glm::vec3 ray_nds = glm::vec3(x, y, z);

		glm::vec4 ray_clip = glm::vec4(x , y, -1.0, 1.0);

	
			
		GLdouble ProjMatrix[16];
		glGetDoublev(GL_PROJECTION_MATRIX, ProjMatrix);

		
		glm::mat4 mProjMatrix = glm::mat4(ProjMatrix[0], ProjMatrix[1], ProjMatrix[2], ProjMatrix[3],
			ProjMatrix[4], ProjMatrix[5], ProjMatrix[6], ProjMatrix[7],
			ProjMatrix[8], ProjMatrix[9], ProjMatrix[10], ProjMatrix[11],
			ProjMatrix[12], ProjMatrix[13], ProjMatrix[14], ProjMatrix[15]);

	 

		//temp tp check proj
		//glm::mat4 tempProj = glm::ortho(-10.0F, 10.0F, -10.0F, 10.0F, 0.1f, 100.0F);  

		GLdouble ViewMatrix[16];
		
		glGetDoublev(GL_MODELVIEW_MATRIX, ViewMatrix);
		

		glm::mat4 mViewMatrix = glm::mat4(ViewMatrix[0], ViewMatrix[1], ViewMatrix[2], ViewMatrix[3],
			ViewMatrix[4], ViewMatrix[5], ViewMatrix[6], ViewMatrix[7],
			ViewMatrix[8], ViewMatrix[9], ViewMatrix[10], ViewMatrix[11],
			ViewMatrix[12], ViewMatrix[13], ViewMatrix[14], ViewMatrix[15]);
		 
		 
		//temp to check view
		//glm::mat4 tempLookAt = glm::lookAt(glm::vec3(0, 6, 0), glm::vec3(0, 0, 0),glm::vec3( 0, 0, 1));

		 
		 
		
		glm::vec4 mRayEye = glm::inverse(mProjMatrix)*ray_clip ;
		
		mRayEye = glm::vec4(mRayEye.x, mRayEye.y, -1.0f, 0.0f);
		
		glm::vec4 temp = glm::inverse(mViewMatrix)*mRayEye;
		glm::vec3 vRayWor = glm::vec3(temp.x,temp.y,temp.z);

		vRayWor = glm::normalize(vRayWor);
		
		glm::vec3  o = glm::vec3 (globalCamera.getCameraPosition().x,
								  globalCamera.getCameraPosition().y, 
								  globalCamera.getCameraPosition().z);
		glm::vec3  d = glm::vec3 (vRayWor.x, vRayWor.y, vRayWor.z);
 

		
		 

		//iterate all mesh object and find the nearest obj
		{
			 
			

			//create temp distance
			float tempMinValue;
			//simple ignore face picking, set tempDistance to max value
			tempMinValue = 100000;

			//iterate manager list
			//create mesh distance
			bool isIntersect = false;
			float meshDistance = 0;
			Node<Mesh>* tempMesh = MeshManager::getHeadOfVertex();
			while (tempMesh) {
			 
				 
				bool isTempIntersect = tempMesh->value->RayABBBIntersect(o, d, meshDistance);
				isIntersect = isIntersect || isTempIntersect;
				if (isTempIntersect&&meshDistance < tempMinValue) {
					MeshManager::selectedMesh = tempMesh->value;
					tempMinValue = meshDistance;
				}
				tempMesh = tempMesh->next;
			}

			//if not intersect
			if (!isIntersect) {
				if (MeshManager::selectedMesh) { 
					MeshManager::selectedMesh->isSelected = false; 
					MeshManager::selectedMesh = nullptr;
				}
				if (MeshManager::selectedFace) {
					MeshManager::selectedFace->isSelected = false;
					MeshManager::selectedFace = nullptr;
				}
				
			}

			//prepare for face pick
			//this mesh is already selected
			//TODO test face picking
			//MeshManager::selectedMesh = MeshManager::list->head->value;
			//MeshManager::selectedMesh->isSelected = true;
		    if (MeshManager::selectedMesh) {
			 	    if (MeshManager::selectedMesh->isSelected&&Mesh_FaceMode == FaceModify) {
					
					if(MeshManager::selectedFace)MeshManager::selectedFace->isSelected = false;
					if (MeshManager::selectedFace)MeshManager::selectedFace = nullptr;
					//face pick
					//ray cast all the face in this mesh
					//TODO remove hard code
					float tempMinDistance = 10000000;
					float faceDistance = 0;
					//Face* tempFace = &MeshManager::selectedMesh->face[0];
					for (int i = 0; i < MeshManager::selectedMesh->numFaces; i++) {
						bool isIntersect = RayPlaneIntersect(o, d, &MeshManager::selectedMesh->face[i], faceDistance);
						if (isIntersect&& faceDistance < tempMinDistance) {
							MeshManager::selectedFace = &MeshManager::selectedMesh->face[i];
							tempMinDistance = faceDistance;
						}
					}

					if (MeshManager::selectedFace) {
						std::cout << "Picking debug" << endl;
						for (int i = 0; i < MeshManager::selectedFace->nVerts; i++) {
							cout << "Vertex index" << MeshManager::selectedFace->vert[i].vertIndex<< " ";
							 
						}
						cout << endl;
						MeshManager::selectedFace->isSelected = true;
					}
					// get the nearest one 
				}
				else {
					tempMesh = MeshManager::getHeadOfVertex();
					while (tempMesh) {
						tempMesh->value->isSelected = false;
						tempMesh = tempMesh->next;

					}
					//TODO check
					if (MeshManager::selectedMesh) MeshManager::selectedMesh->isSelected = true;
					if (MeshManager::selectedFace)MeshManager::selectedFace->isSelected = false;
					 
				}

			}
			//if distance <tempDistance MeshManager::selectedMesh = this mesh and tempDistance = distance


			 
		}
	 
		///
		//glm::vec4 ray_eye = glm::inverse();
	}

	firstMouse = true;
	
	glutPostRedisplay();

	 
}


void mouseMotion(int xpos, int ypos) {
	if (firstMouse)
	{
		lastX = xpos;
		lastY = ypos;
		firstMouse = false;
	}
	if (MeshManager::selectedMesh != nullptr) {
	
		////cout << "translate log: "<<xpos<<"  "<<ypos  << endl;
		GLdouble modelViewMatrix[16];
		glGetDoublev(GL_MODELVIEW_MATRIX, modelViewMatrix);
		GLdouble projectionMatrix[16];
		glGetDoublev(GL_PROJECTION_MATRIX, projectionMatrix);
		GLint viewMatrix[4];
		glGetIntegerv(GL_VIEWPORT, viewMatrix);
		GLdouble *x, *y, *z;
		x = new GLdouble();
		y = new GLdouble();
		z = new  GLdouble();
		//TODO 
		GLdouble *oldx, *oldy, *oldz;
		oldx = new GLdouble();
		oldy = new GLdouble();
		oldz = new  GLdouble();
		//cout << "Screen coordinate :" << xpos << " " << ypos << endl;
		//cout << "Screen coordinate -last :" << lastX<< " " << lastY << endl;
		gluUnProject(xpos, ypos, 0, modelViewMatrix, projectionMatrix, viewMatrix, x, y, z);
		gluUnProject(lastX, lastY, 0, modelViewMatrix, projectionMatrix, viewMatrix, oldx, oldy, oldz);
		if (x != nullptr&&y != nullptr&&z != nullptr) {
			//cout << "Old In world space  " << *oldx << " " << *oldy << " " << *oldz << endl;
			//cout << "In world space  " << *x << " " << *y << " " << *z << endl;
		}
	
		lastX = xpos;
		lastY = ypos;
		//the screen coordinate is upside down => oldY - y
		glm::vec3 transVector = glm::vec3(*x - *oldx, *oldy - *y, *z - *oldz);
		//multiple transvector with k to map the mesh with mouse in screen
		//TODO change the algorithm, the length function is time complexity
		glm::vec3 cameraPosition = globalCamera.getCameraPosition();
		//cout << "Trans vector : " << transVector.x << " "<< transVector.y <<" " <<transVector.z << endl;
		float k = 1.f;
		//float kleft = MeshManager::selectedMesh->distance(cameraPosition);
		//float kRight = length((glm::vec3(*oldx, *oldy, *oldz) - cameraPosition));
		
		//k = kleft / kRight;
		////cout << "k = " << k << endl;
		transVector = k*transVector;
		//  translate
		glm::mat4 transMatrix = glm::mat4(1);
		if (InputMode == Translate) {
			cout << "Translate Mode" << endl;
			transVector = glm::inverse(MeshManager::selectedMesh->transMatrix)*glm::vec4(transVector, 0);
			if (MeshManager::selectedFace != nullptr) {
				
				transformFace(glm::translate(transMatrix, transVector));
			}
			else
				MeshManager::selectedMesh->transMatrix = glm::translate(MeshManager::selectedMesh->transMatrix, transVector);
			
		}
	//TODO rotate
		else if (InputMode == Rotate) {
			cout << "Rotate Mode" << endl;
			float transProjectRight = glm::dot(transVector, globalCamera.getCameraRight());
			float transProjectUp = glm::dot(transVector, globalCamera.getCameraUp());
			//cout << "Global camera right vector " << globalCamera.getCameraRight().x << " " << globalCamera.getCameraRight().y << " " << globalCamera.getCameraRight().z << endl;
			if (abs(transProjectRight) > abs(transProjectUp))
			{
				cout << "Float :" << transProjectRight << endl;
				if (transProjectRight > 0)
					transVector = -glm::inverse(MeshManager::selectedMesh->transMatrix)*glm::vec4(globalCamera.getCameraUp(),0);
				else
					transVector = glm::inverse(MeshManager::selectedMesh->transMatrix)*glm::vec4(globalCamera.getCameraUp(), 0);
			}
			else {
				cout << "Float :" << transProjectUp << endl;
				if (transProjectUp < 0)
					transVector = -glm::inverse(MeshManager::selectedMesh->transMatrix)*glm::vec4(globalCamera.getCameraRight(), 0);
				else
					transVector = glm::inverse(MeshManager::selectedMesh->transMatrix)*glm::vec4(globalCamera.getCameraRight(), 0);
			}
			cout << "Transvector = " << transVector.x << " " << transVector.y << " " << transVector.z << endl;
			if (MeshManager::selectedFace != nullptr) {

				transformFace(glm::rotate(transMatrix, glm::radians(5.0f), transVector));
			}else
				MeshManager::selectedMesh->transMatrix = glm::rotate(MeshManager::selectedMesh->transMatrix, glm::radians(5.0f), transVector);
		}
		else if (InputMode == Scale) {
			//TODO
			cout << "Scale Mode" << endl;
			float x = (2.0f * xpos) / screenWidth - 1.0f;
			float y = (2.0f * (screenHeight - ypos)) / screenHeight - 1.0f;

			////cout << x << "    " << y << endl;
			float z = 1.0f;
			//glm::vec3 ray_nds = glm::vec3(x, y, z);

			glm::vec4 ray_clip = glm::vec4(x, y, -1.0, 1.0);



			GLdouble ProjMatrix[16];
			glGetDoublev(GL_PROJECTION_MATRIX, ProjMatrix);


			glm::mat4 mProjMatrix = glm::mat4(ProjMatrix[0], ProjMatrix[1], ProjMatrix[2], ProjMatrix[3],
				ProjMatrix[4], ProjMatrix[5], ProjMatrix[6], ProjMatrix[7],
				ProjMatrix[8], ProjMatrix[9], ProjMatrix[10], ProjMatrix[11],
				ProjMatrix[12], ProjMatrix[13], ProjMatrix[14], ProjMatrix[15]);



			//temp tp check proj
			//glm::mat4 tempProj = glm::ortho(-10.0F, 10.0F, -10.0F, 10.0F, 0.1f, 100.0F);  

			GLdouble ViewMatrix[16];

			glGetDoublev(GL_MODELVIEW_MATRIX, ViewMatrix);


			glm::mat4 mViewMatrix = glm::mat4(ViewMatrix[0], ViewMatrix[1], ViewMatrix[2], ViewMatrix[3],
				ViewMatrix[4], ViewMatrix[5], ViewMatrix[6], ViewMatrix[7],
				ViewMatrix[8], ViewMatrix[9], ViewMatrix[10], ViewMatrix[11],
				ViewMatrix[12], ViewMatrix[13], ViewMatrix[14], ViewMatrix[15]);


			//temp to check view
			//glm::mat4 tempLookAt = glm::lookAt(glm::vec3(0, 6, 0), glm::vec3(0, 0, 0),glm::vec3( 0, 0, 1));




			glm::vec4 mRayEye = glm::inverse(mProjMatrix)*ray_clip;

			mRayEye = glm::vec4(mRayEye.x, mRayEye.y, -1.0f, 0.0f);

			glm::vec4 temp = glm::inverse(mViewMatrix)*mRayEye;
			glm::vec3 vRayWor = glm::vec3(temp.x, temp.y, temp.z);

			vRayWor = glm::normalize(vRayWor);

			glm::vec3  o = glm::vec3(globalCamera.getCameraPosition().x,
				globalCamera.getCameraPosition().y,
				globalCamera.getCameraPosition().z);
			glm::vec3  d = glm::vec3(vRayWor.x, vRayWor.y, vRayWor.z);





			//iterate all mesh object and find the nearest obj
			{



				//create temp distance
				float tempMinValue;
				//simple ignore face picking, set tempDistance to max value
				tempMinValue = 100000;

				//iterate manager list
				//create mesh distance
				bool isIntersect = false;
				float meshDistance = 0;
				Node<Mesh>* tempMesh = MeshManager::list->head;
				int i = 0;
				int intersectAxis = -1;
				while (i < 3) {


					bool isTempIntersect = tempMesh->value->RayABBBIntersect(o, d, meshDistance);
					isIntersect = isIntersect || isTempIntersect;
					if (isTempIntersect&&meshDistance < tempMinValue) {
				 
						tempMinValue = meshDistance;
						intersectAxis = i;
					}
					tempMesh = tempMesh->next;
					i++;
				}
				float scaleRate = 0.1;
				cout << "Axis" << intersectAxis;
				if (intersectAxis != -1) {
					if (intersectAxis == 0) {
						float direct = glm::dot(transVector, glm::vec3(MeshManager::selectedMesh->transMatrix*glm::vec4(1, 0, 0,0)));
						direct = (direct > 0) ? 1 : -1;
						transVector = glm::vec3(1 + direct*scaleRate, 1, 1);
					}
					else if (intersectAxis == 1) {
						float direct = glm::dot(transVector, glm::vec3(MeshManager::selectedMesh->transMatrix*glm::vec4(0, 1, 0, 0)));
						direct = (direct > 0) ? 1 : -1;
						transVector = glm::vec3(1, 1 + direct*scaleRate, 1);
					}
					else {
						float direct = glm::dot(transVector, glm::vec3(MeshManager::selectedMesh->transMatrix*glm::vec4(0, 0, 1, 0)));
						direct = (direct > 0) ? 1 : -1;
						transVector = glm::vec3(1, 1, 1 + direct*scaleRate);
					}
					if (MeshManager::selectedFace != nullptr) {

						transformFace(glm::scale(transMatrix, transVector));
					}
					else
						MeshManager::selectedMesh->transMatrix = glm::scale(MeshManager::selectedMesh->transMatrix, transVector);
				}
			}
		}

		glutPostRedisplay();
		//TODO scale
	}
	else {
		

		float xoffset = xpos - lastX;
		float yoffset = lastY - ypos;
		lastX = xpos;
		lastY = ypos;

		float sensitivity = 0.05;
		xoffset *= sensitivity;
		yoffset *= sensitivity;

		yaw += xoffset;
		pitch += yoffset;

		if (pitch > 89.0f)
			pitch = 89.0f;
		if (pitch < -89.0f)
			pitch = -89.0f;

		glm::vec3 front;
		front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
		front.y = sin(glm::radians(pitch));
		front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
		globalCamera.setCameraFront(glm::normalize(front));
		glutPostRedisplay();
	}
}
 

bool RaySphereIntersect(glm::vec3 &o, glm::vec3 &d, glm::vec3 &c, float r, float& dis) {
	//vector from ray origin to the center of sphere
	glm::vec3 _l = glm::vec3((c - o).x, (c - o).y, (c - o).z);
	
	glm::vec3 _d = glm::vec3(d.x, d.y, d.z);

	//projection of l onto ray
	float s = glm::dot(_l,_d);

	// distance from origin to sphere center
	float l2 = glm::dot(_l,_l);
	
	
	float r2 = r*r;
	
	// the sphere is behind ray origin
	if (s<0 && l2>r2) return  false;

	// distance from sphere origin to ray
	float m2 = l2 - s*s;
	
	//  ray misses sphere
	if (m2 > r2) return false;

	// 
	float q = sqrt(r2 - m2);
	
	// if ray origin is in sphere
	if (l2 > r2) dis = s - q;
	// out of sphere
	else dis = s + q;
	return true;


}


void transformFace(glm::mat4& transMatrix) {
	for (int v = 0; v < MeshManager::selectedFace->nVerts; v++)
			{

				int		iv = MeshManager::selectedFace->vert[v].vertIndex;
				 
				//get the vertex
				glm::vec4 temp = glm::vec4(MeshManager::selectedMesh->pt[iv].x, MeshManager::selectedMesh->pt[iv].y, MeshManager::selectedMesh->pt[iv].z, 1);
				//transform vertex
				temp = transMatrix*temp;
				// set the vertex
				MeshManager::selectedMesh->pt[iv].x = temp.x;
				MeshManager::selectedMesh->pt[iv].y = temp.y;
				MeshManager::selectedMesh->pt[iv].z = temp.z;
				//TODO calculate the AABB box

			}
	//transform AABB
	MeshManager::selectedMesh->EmptyAABB();
	MeshManager::selectedMesh->addAllPoint();
}

//hard code MeshManager::selectedMesh
bool checkPointInsideFace(glm::vec3 &o, Face* face) {
	bool temp = false;
	for (int i = 0; i < face->nVerts-1; i+=2) {
		temp = temp || checkPointInsideRect(o, MeshManager::selectedMesh->pt[face->vert[i].vertIndex], MeshManager::selectedMesh->pt[face->vert[i + 1].vertIndex], MeshManager::selectedMesh->pt[face->vert[(i + 2)%face->nVerts].vertIndex]);
	}
	return temp;
}
//is x,y same side 
bool SameSide(glm::vec3 x, glm::vec3 y, glm::vec3 m, glm::vec3 n) {
	glm::vec3 cp1 = glm::cross(n - m, x - m);
	glm::vec3 cp2 = glm::cross(n - m, y - m);
	if (glm::dot(cp1, cp2) >= 0) return true;
	else
		return false;
}
bool checkPointInsideRect(glm::vec3 &o, glm::vec3 &x, glm::vec3 &y, glm::vec3 &z) {
	glm::mat4 trans = MeshManager::selectedMesh->transMatrix;
	glm::vec3 _o = glm::vec3(o.x, o.y, o.z);
	glm::vec3 _x = glm::vec3(trans*glm::vec4(x.x, x.y, x.z, 1));
	glm::vec3 _y = glm::vec3(trans*glm::vec4(y.x, y.y, y.z, 1));
	glm::vec3 _z = glm::vec3(trans*glm::vec4(z.x, z.y, z.z, 1));
	if (SameSide(_o, _x, _y, _z) && SameSide(_o, _y, _x, _z) && SameSide(_o, _z, _x, _y)) return true;
	else return false; 
}


//the mesh which have this face is already known (MeshManager::selectedMesh)
bool RayPlaneIntersect(glm::vec3 &o, glm::vec3 &d, Face* checkFace, float& distance) {
	// p(t) = p0 +t*d
	// p.n = dn
	// t = (dn-p0.n)/(d.n)
	// if d.n = 0 => the ray is parallel to the plane
	 
	 

	// TODO remove the useless code
	glm::vec4 localSpace0 = glm::vec4(MeshManager::selectedMesh->pt[checkFace->vert[0].vertIndex].x, MeshManager::selectedMesh->pt[checkFace->vert[0].vertIndex].y, MeshManager::selectedMesh->pt[checkFace->vert[0].vertIndex].z, 1);
	 
	glm::mat4 trans = MeshManager::selectedMesh->transMatrix;
	glm::vec4 vertex0InWorldSpace = trans*localSpace0;
	glm::vec3 _n = glm::vec3(0, 0, 0);
	// 
	////Test
	//Mesh mesh;
	//mesh.numVerts = 3;
	//mesh.pt = new glm::vec3[3];
	//mesh.pt[0] = glm::vec3(6, 1, 4);
	//mesh.pt[1] = glm::vec3(7, 0, 9);
	//mesh.pt[2] = glm::vec3(1, 1, 2);
	//glm::vec3 _n = glm::vec3(0,0,0);
	//for (int i = 0; i < mesh.numVerts; i++) {
	//	glm::vec4 first = trans*glm::vec4(mesh.pt[i],1);
	//	glm::vec4 sec = trans*glm::vec4(mesh.pt[(i + 1) % mesh.numVerts], 1);

	//	_n.x += (first.y - sec.y)*(first.z + sec.z);
	//	_n.y += (first.z - sec.z)*(first.x + sec.x);
	//	_n.z += (first.x - sec.x)*(first.y + sec.y);

	//}

	////cout << "test" << _n.x << _n.y << _n.z;
	// 
	for (int i = 0; i < checkFace->nVerts; i++) {
		glm::vec4 first = trans*glm::vec4(MeshManager::selectedMesh->pt[checkFace->vert[i].vertIndex], 1);
		glm::vec4 sec = trans*glm::vec4(MeshManager::selectedMesh->pt[checkFace->vert[(i + 1) % checkFace->nVerts].vertIndex], 1);

		_n.x += (first.y - sec.y)*(first.z + sec.z);
		_n.y += (first.z - sec.z)*(first.x + sec.x);
		_n.z += (first.x - sec.x)*(first.y + sec.y);

	}
	 

	glm::vec3 _p = vertex0InWorldSpace;
	float dn = glm::dot(_p, _n);

	if (glm::dot(d, _n)) {
	
		float _t = (dn - glm::dot(o, _n)) / (glm::dot(d, _n));
		//check if the intersect point is inside face
		glm::vec3 intersectPoint = o + _t*d;
		 
		bool isInsde = checkPointInsideFace(intersectPoint, checkFace);
		if (isInsde) {
			distance = _t;
			return true;
		}
		else{
			return false;
		}
		 
	}
	else
		return false;
	

}