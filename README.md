<h1>This is a picking project for my OpenGL class's assignment.</h1>
<h2>Refactoring</h2>
- Xóa bỏ toàn bộ mọi class trong supportClass bằng các class trong thư viện glm >> Hoàn thành
- Thêm class camera.   >> Hoàn thành.
- Thêm class MeshManager để quản lý mesh. >> Done!
- Chỉnh sửa cách input
- Thay thế hàm dùng để tính normal của face.
- Chỉnh sửa các hàm intersect
<h2>Add features</h2>
- Thêm ánh sáng
- Thêm texture
- Translate mesh bằng chuột.
- Scale mesh bằng chuột
- Rotate mesh bằng chuột.
- Delete mesh
- Thêm các nguồn sáng bằng tay
\\

//BUG NOTE
- Có một lỗi không pick được mesh với cube ở vị trí có trans = (3,3,0);
	- Chỉ pick được khi chọn vào mặt trước của vât
	- Không pick được khi chon các mặt hai bên.
	- Vẫn pick được bình thường ở các vị trị đối xứng như (-3,-3,0).